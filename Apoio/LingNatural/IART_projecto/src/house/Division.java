package house;

import java.util.ArrayList;
import java.util.Scanner;



public class Division {
	
	public static final int MAXIMUM_VALUE = 100;
	public static final int MINIMUM_VALUE = 0;
	public static final int HUMIDITY_AVERAGE = 70;
	public static final int TEMPERATURE_AVERAGE = 10;
	
	
	private ArrayList<Integer> lightIntensity = new ArrayList<Integer>(); // 0 mean that it's off
	private boolean movementSensor = false;
	private ArrayList<Boolean> windows = new ArrayList<Boolean>(); // true means that it's open
	private ArrayList<Boolean> doors = new ArrayList<Boolean>(); // true means that it's open
	private double humidity = HUMIDITY_AVERAGE;
	private double temperature = TEMPERATURE_AVERAGE;
	private String name;
	
	private ArrayList<Appliance> appliances = new ArrayList<Appliance>();
	
	public  Division(int numberOfWindows, int numberOfDoors, int numberOfLights, String name){
		//System.out.println("Construtor de division.");
		this.setName(name);
		for(int i = 0; i < numberOfWindows ; i++){
			windows.add(false);
		}
		for(int i = 0; i < numberOfDoors ; i++){
			doors.add(false);
		}
		for(int i = 0; i < numberOfLights; i++){
			getLightIntensity().add(0);
		}
		//addAppliances();
	}
	
	public void addAppliances(){
		String name="";
		while(!name.equals("quit")){
			System.out.println("\t- Enter the name of the appliance: (quit to stop entering actions) ");
			name = new Scanner(System.in).nextLine();
			appliances.add(new Appliance(name));
		}
	}

	public boolean isMovement() {
		return movementSensor;
	}

	public void setMovement(boolean movementSensor) {
		this.movementSensor = movementSensor;
	}

	public double getHumidity() {
		return humidity;
	}

	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}

	public double getTemperature() {
		return temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	public ArrayList<Appliance> getAppliances() {
		return appliances;
	}

	public void setAppliances(ArrayList<Appliance> appliances) {
		this.appliances = appliances;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void modifyLights(String object, String acao) {
		Integer intensity = Integer.parseInt(acao);
		System.out.println("Object: " + object);
		System.out.println("Intensity: " + intensity);
		
		System.out.println("Size: " + getLightIntensity().size());
		String[] str = object.split(" ");
		
		if(object.equals("all"))
			for(int i = 0; i < getLightIntensity().size(); i++)
				lightIntensity.set(i, intensity);
		else{
			Integer lightNo = Integer.parseInt(str[1]);
			System.out.println(lightNo);
			
			lightIntensity.set(lightNo, intensity);
		}
		for(int i = 0; i < getLightIntensity().size(); i++)
			System.out.println(getLightIntensity().get(i));
		
	}

	public void modifyAppliances(String object, String acao) {
		boolean on_off = false;
		if(acao.equals("on"))
			on_off = true;
		
		if(object.equals("all"))
			for(Appliance a : appliances)
				a.setState(on_off);
		else{
			for(int i = 0; i < appliances.size(); i++)
				if(appliances.get(i).getName().equals(object)){
					appliances.get(i).setState(on_off);
					break;
				}
		}
	}

	public ArrayList<Integer> getLightIntensity() {
		return lightIntensity;
	}

	public void setLightIntensity(ArrayList<Integer> lightIntensity) {
		this.lightIntensity = lightIntensity;
	}

	public void addAppliance(String appliance) {
		this.appliances.add(new Appliance(appliance));
		
	}

	public boolean hasAppliance(String appliance) {
		for(Appliance a : appliances){
			if(a.getName().equals(appliance)){
				return true;
			}
		}
		return false;
	}

	public ArrayList<Boolean> getDoors() {
		return doors;
	}

	public ArrayList<Boolean> getWindows() {
		// TODO Auto-generated method stub
		return windows;
	}

	public void modifyDoors(String object, String acao) {
		System.out.println("Object: " + object);
		System.out.println("Accao: " + acao);
		
		if(object.equals("all")){
			System.out.println();
			for(int i = 0; i < this.doors.size(); i++)
				if(acao.equals("open")){
					doors.set(i, true);
				}
				else{
					doors.set(i, false);
				}
		}else{
			int doorNo = Integer.parseInt(object.split(" ")[1]);
			System.out.println("Door No: " + doorNo);
			
			if(acao.equals("open"))
				doors.set(doorNo, true);
			else
				doors.set(doorNo, false);
		}
		
	}

	public void modifyWindows(String object, String acao) {
		
		if(object.equals("all")) {
			System.out.println("changing all windows.");
			for(int i = 0; i < this.windows.size(); i++)
				if(acao.equals("open")){
					windows.set(i, true);
				}
				else
					windows.set(i, false);
		}else{
			int doorNo = Integer.parseInt(object.split(" ")[1]);
			System.out.println("Window No: " + doorNo);
			
			if(acao.equals("open"))
				windows.set(doorNo, true);
			else
				windows.set(doorNo, false);
		}
		
	}
}
