package house;

public class Appliance {

	private String name;
	private boolean state;//true means it's on
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean getState() {
		return state;
	}
	public void setState(boolean state) {
		this.state = state;
		System.out.println(name + " turned " + state);
	}
	
	public Appliance(String name){
		this.name = name;
		this.state = false;
	}
}
