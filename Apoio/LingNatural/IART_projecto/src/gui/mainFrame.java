package gui;

import house.Appliance;
import house.Division;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JComboBox;
import java.awt.FlowLayout;
import javax.swing.JSpinner;
import java.awt.GridLayout;
import java.awt.Label;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.DefaultTableModel;

import jess.JessException;

import logic.houseManager;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Color;
import java.util.Timer;
import java.util.TimerTask;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class mainFrame extends JFrame {
	private static final int MINIMUM_TIMER_TIME = 50000;
	private houseManager manager;
	private JPanel contentPane;
	JSpinner spinnerTemperature;
	private JLabel timeLabel;
	static Timer timer = new Timer("JessUpdater");
	private static JTable table;
	ButtonGroup group;
	JRadioButton rdBtnEmpty;
	JRadioButton rdBtnOccupied;
	JButton btnStart;
	private JSpinner spinnerHumidity;
	private JLabel dayStageLabel;
	private JSpinner indoorTempSpinner;
	private static mainFrame thisFrame;
	private JSlider slider;
	private int taskTimeUpdater = 1000;
	private JLabel updateTimeLabel;
	private JSpinner spinnerLuminosity;
	

	/**
	 * Launch the application.
	 * @throws JessException 
	 */
	public static void main(String[] args) throws JessException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					mainFrame frame = new mainFrame();
					
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void notify(String object, String action, int value) {
		System.out.println("[GUI] Object: " + object);
		System.out.println("[GUI] Action:" + action);
		if (value == -1) {
			// change status of object to action
		} else {
			System.out.println("[GUI] Value: " + value);
			// Apply action to object on value units
			// Example: decrease kitchen light 3 units
		}

	}

	/**
	 * Create the frame.
	 * 
	 * @throws JessException
	 */
	public mainFrame() throws JessException {
		thisFrame = this;
		
		manager = houseManager.getManager();
		Division d = new Division(1,1,1,"main room");
		d.getAppliances().add(new Appliance("coffe-machine"));
		d.getAppliances().add(new Appliance("A/C"));
		manager.getDivisions().add(d);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		Label temperatureLabel = new Label("Temperature");
		temperatureLabel.setBounds(10, 29, 75, 22);
		contentPane.add(temperatureLabel);

		spinnerTemperature = new JSpinner();
		spinnerTemperature.setLocation(113, 31);
		spinnerTemperature.setSize(60, 20);
		spinnerTemperature.setValue(15);
		spinnerTemperature.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				try {
					double v = Double.parseDouble(spinnerTemperature.getValue().toString());
					//v = (double)Math.round(v * 10) / 10;		
					manager.updateTemperature(v);
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (JessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		contentPane.add(spinnerTemperature);

		JLabel lblHour = new JLabel(" Hour");
		lblHour.setBounds(10, 11, 75, 14);
		contentPane.add(lblHour);

		Label humidityLabel = new Label("Humidity:");
		humidityLabel.setBounds(193, 54, 54, 22);
		contentPane.add(humidityLabel);

		timeLabel = new JLabel("00:00");
		timeLabel.setBounds(91, 11, 46, 14);
		contentPane.add(timeLabel);

		JLabel lblSlow = new JLabel("Slow");
		lblSlow.setBounds(158, 11, 42, 14);
		contentPane.add(lblSlow);

		JLabel lblFast = new JLabel("Fast");
		lblFast.setBounds(426, 11, 48, 14);
		contentPane.add(lblFast);

		updateTimeLabel = new JLabel("1");
		updateTimeLabel.setBounds(428, 29, 46, 14);
		contentPane.add(updateTimeLabel);
		
		slider = new JSlider();
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				double dif = slider.getValue();
				
				dif = dif / 50;
				dif = Math.pow(dif, -1);
				if(slider.getValue() == 0)
					dif = MINIMUM_TIMER_TIME;
				
				dif = (double)Math.round(dif * 10) / 10;		
				
				updateTimeLabel.setText(Double.toString(dif));
				taskTimeUpdater = (int) (1000 * dif);
			}
		});
		slider.setBounds(198, 11, 200, 14);
		contentPane.add(slider);

		spinnerHumidity = new JSpinner();
		spinnerHumidity.setBounds(253, 56, 45, 20);
		contentPane.add(spinnerHumidity);

		JPanel housepanel = new JPanel();
		housepanel.setBounds(10, 107, 464, 194);
		contentPane.add(housepanel);
		housepanel.setLayout(null);

		JLabel lblHouse = new JLabel("House");
		lblHouse.setBounds(10, 11, 57, 14);
		housepanel.add(lblHouse);

		JButton btnRules = new JButton("Add Rule");
		btnRules.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				new AddRule();
			}
		});
		btnRules.setBounds(10, 36, 109, 23);
		housepanel.add(btnRules);

		JButton btnShowRules = new JButton("Show Rules");
		btnShowRules.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				new ShowRule();
			}
		});
		btnShowRules.setBounds(10, 59, 109, 23);
		housepanel.add(btnShowRules);

		JButton btnDeleteRule = new JButton("Delete Rule");
		btnDeleteRule.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new DeleteRule();
			}
		});
		btnDeleteRule.setBounds(10, 82, 109, 23);
		housepanel.add(btnDeleteRule);

		JButton btnAddDivision = new JButton("Add Division");
		btnAddDivision.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new AddDivision();
			}
		});
		btnAddDivision.setFont(new Font("Tahoma", Font.PLAIN, 10));
		btnAddDivision.setBounds(10, 114, 109, 23);
		housepanel.add(btnAddDivision);

		JButton btnDeleteDivision = new JButton("Delete Division");
		btnDeleteDivision.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				new DeleteDivision();
			}
		});
		btnDeleteDivision.setFont(new Font("Tahoma", Font.PLAIN, 8));
		btnDeleteDivision.setBounds(10, 139, 109, 23);
		housepanel.add(btnDeleteDivision);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(140, 11, 314, 172);
		housepanel.add(panel_1);
		panel_1.setLayout(new GridLayout(1, 0, 0, 0));

		JPanel divisions = new JPanel();
		panel_1.add(divisions);
		divisions.setLayout(null);
		
		table = new JTable(new DefaultTableModel(new Object[] { "Division" }, 0));
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(arg0.KEY_PRESSED == 401){
					if(table.getSelectedRow() == -1){
						JOptionPane.showMessageDialog(thisFrame, "Please select a valid row.");
						return;
					}
					try {
						new ShowDivision(table.getSelectedRow());
					} catch (JessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
			}
		});
		
		DefaultTableModel tableModel = new DefaultTableModel(new Object[][] {},
				new String[] { "Division" }) {

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		
		table.setModel(tableModel);
		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(10, 11, 294, 150);
		divisions.add(pane);
		
		btnStart = new JButton("Start ");
		btnStart.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				slider.setEnabled(false);
				timer.schedule(new Tarefa(), 0, taskTimeUpdater);
				btnStart.setText("Started.");
				btnStart.setEnabled(false);
			}
		});
		btnStart.setBounds(20, 173, 89, 23);
		housepanel.add(btnStart);
		group = new ButtonGroup();
		
		rdBtnEmpty = new JRadioButton("Empty");
		rdBtnEmpty.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if(rdBtnEmpty.isSelected())
					try {
						houseManager.getManager().setHouseEmpty();
					} catch (JessException e) {}
				else
					try {
						houseManager.getManager().setHouseOccupied();
					} catch (JessException e) {}
			}
		});
		
		group.add(rdBtnEmpty);
		rdBtnEmpty.setBounds(194, 80, 74, 23);
		contentPane.add(rdBtnEmpty);
		
		rdBtnOccupied = new JRadioButton("Occupied");
		rdBtnOccupied.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(rdBtnOccupied.isSelected())
					try {
						houseManager.getManager().setHouseOccupied();
					} catch (JessException e1) {}
				else
					try {
						houseManager.getManager().setHouseEmpty();
					} catch (JessException e1) {}
			}
		});
		rdBtnOccupied.setBounds(270, 80, 88, 23);
		rdBtnOccupied.setSelected(true);
		group.add(rdBtnOccupied);
		contentPane.add(rdBtnOccupied);
		
		
		JLabel lblHouse_1 = new JLabel("House:");
		lblHouse_1.setBounds(142, 85, 46, 14);
		contentPane.add(lblHouse_1);
		
		dayStageLabel = new JLabel("Day Stage: MORNING");
		dayStageLabel.setBounds(10, 85, 126, 14);
		contentPane.add(dayStageLabel);
		
		JLabel lblInsideTemperature = new JLabel("Temperature indoor:");
		lblInsideTemperature.setBounds(10, 60, 126, 14);
		contentPane.add(lblInsideTemperature);
		
		indoorTempSpinner = new JSpinner();
		indoorTempSpinner.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				double v = Double.parseDouble(indoorTempSpinner.getValue().toString());
				v = (double)Math.round(v * 10) / 10;		
				try {
					manager.getJessM().setInsideTemperature(v);
				} catch (JessException e) {
					e.printStackTrace();
				}
			}
		});
		indoorTempSpinner.setBounds(128, 57, 45, 20);
		contentPane.add(indoorTempSpinner);

		
		fillTable();
		
		this.setVisible(true);
		indoorTempSpinner.setValue(manager.getJessM().getInsideTemperature());
		
		JLabel lblUpdateTimems = new JLabel("Update Time (s)");
		lblUpdateTimems.setBounds(311, 29, 106, 14);
		contentPane.add(lblUpdateTimems);
		
		spinnerLuminosity = new JSpinner();
		spinnerLuminosity.setBounds(409, 56, 45, 20);
		contentPane.add(spinnerLuminosity);
		
		Label label = new Label("Luminosity:");
		label.setBounds(321, 54, 73, 22);
		contentPane.add(label);
		
		
		//timer.schedule(new Tarefa(), 0, 1000);
	}

	private class Tarefa extends TimerTask {
		

		@Override
		public void run() {
			manager.setMinutes(manager.getJessM().getMinutes() + 1);
			try {
				manager.update();
				manager.getJessM().update();
			} catch (JessException e) {

				e.printStackTrace();
			}
			if (manager.getMinutes() >= 60) {

				manager.setMinutes(0);
				manager.setHours(manager.getHours() + 1);
				/*if (manager.getHours() == 24) {
					*manager.saveActions();
					this.cancel();
				}*/
			}
			timeLabel.setText(manager.getJessM().getHours() + ":" + manager.getJessM().getMinutes());
			
		}
	}
	
	public static void fillTable() {
		DefaultTableModel model = ((DefaultTableModel) table.getModel());
		for (int i = model.getRowCount() - 1; i >= 0; i--) {
			model.removeRow(i);
		}
		try {
			for(Division a : houseManager.getManager().getDivisions()){
				model.addRow(new Object[]{a.getName()});
			}
		} catch (JessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public static JFrame getFrame(){
		return thisFrame;
	}
	
	public void updateTemperature(double outsideTemperature) {
		this.spinnerTemperature.setValue(outsideTemperature);
		
	}
	
	public void updateInsideTemperature(double insideTemperature) {
		indoorTempSpinner.setValue(insideTemperature);
	}

	public void updateLuminosity(int outsideLuminosity) {
		this.spinnerLuminosity.setValue(outsideLuminosity);
		
	}

	public void updateHumidity(double outsideHumidity) {
		this.spinnerHumidity.setValue(outsideHumidity);
		
	}
	
	public void updateStage(String stage){
		this.dayStageLabel.setText("Day Stage: " + stage);
	}

	public static void showAlert(String string) {
		JOptionPane.showMessageDialog(thisFrame, string);
		
	}
}
