package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Label;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import jess.JessException;

import logic.houseManager;

public class AddDivision extends JFrame {

	JButton btnSave;
	private JPanel contentPane;
	private JTextField textFieldName;
	private JTextField textFieldWindows;
	private JTextField textFieldDoors;
	private JTextField textFieldLights;
	private AddDivision thisFrame;
	JButton btnAddAppliances;

	/**
	 * Create the frame.
	 */
	public AddDivision() {
		thisFrame = this;
		
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 320, 217);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Label label = new Label("Division name: ");
		label.setBounds(10, 10, 149, 22);
		contentPane.add(label);
		
		textFieldName = new JTextField();
		textFieldName.setBounds(165, 10, 129, 20);
		contentPane.add(textFieldName);
		textFieldName.setColumns(10);
		
		textFieldWindows = new JTextField();
		textFieldWindows.setColumns(10);
		textFieldWindows.setBounds(165, 41, 129, 20);
		contentPane.add(textFieldWindows);
		
		Label label_1 = new Label("Number of windows: ");
		label_1.setBounds(10, 38, 149, 22);
		contentPane.add(label_1);
		
		textFieldDoors = new JTextField();
		textFieldDoors.setColumns(10);
		textFieldDoors.setBounds(165, 72, 129, 20);
		contentPane.add(textFieldDoors);
		
		Label label_2 = new Label("Number of doors: ");
		label_2.setBounds(10, 69, 149, 22);
		contentPane.add(label_2);
		
		textFieldLights = new JTextField();
		textFieldLights.setColumns(10);
		textFieldLights.setBounds(165, 103, 129, 20);
		contentPane.add(textFieldLights);
		
		Label label_3 = new Label("Number of lights: ");
		label_3.setBounds(10, 101, 149, 22);
		contentPane.add(label_3);
		
		btnAddAppliances = new JButton("Add appliances");
		btnAddAppliances.setEnabled(false);
		btnAddAppliances.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(textFieldName.getText().equals("")){
					JOptionPane.showMessageDialog(thisFrame, "Please specify the name of the division first.");
					return;
				}
				new AddAppliance(textFieldName.getText());
				mainFrame.fillTable();
			}
		});
		btnAddAppliances.setBounds(92, 145, 129, 23);
		contentPane.add(btnAddAppliances);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisFrame.setVisible(false);
				thisFrame.dispose();
			}
		});
		
		btnCancel.setBounds(7, 145, 74, 23);
		contentPane.add(btnCancel);
		
		btnSave = new JButton("Save");
		btnSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(textFieldWindows.getText().equals("") || textFieldDoors.getText().equals("") || textFieldLights.getText().equals("") || textFieldName.getText().equals("")){
					JOptionPane.showMessageDialog(thisFrame, "Please fill all the spaces.");
					return;
				}
				btnSave.setEnabled(false);
				try {
					if(!houseManager.getManager().addDivision(textFieldName.getText(), Integer.parseInt(textFieldWindows.getText()), Integer.parseInt(textFieldDoors.getText()), Integer.parseInt(textFieldLights.getText())))
						JOptionPane.showMessageDialog(thisFrame, "Division already exists.");
				} catch (NumberFormatException | JessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				thisFrame.btnAddAppliances.setEnabled(true);
				
				
			}
		});
		btnSave.setBounds(228, 145, 66, 23);
		contentPane.add(btnSave);
		
		this.addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosed(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosing(WindowEvent arg0) {
				thisFrame.setVisible(false);
				thisFrame.dispose();
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

		});
		
		this.setVisible(true);
	}
}
