package gui;

import house.Appliance;
import house.Division;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JButton;

import jess.JessException;

import logic.houseManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;

public class ShowDivision extends JFrame {

	private JPanel contentPane;
	private static JTable tableLights;
	private static JTable tableWindows;
	private static JTable tableDoors;
	private static JTable tableAppliances;
	private ShowDivision thisFrame;

	public static boolean showing = false;

	/**
	 * Create the frame.
	 * @throws JessException 
	 */
	public ShowDivision(int divID) throws JessException {
		showing = true;
		thisFrame = this;
		Division d = houseManager.getManager().getDivisionByID(divID);
		//System.out.println(d.getName());
		
		setBounds(100, 100, 455, 520);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblLights = new JLabel("Lights");
		lblLights.setBounds(10, 11, 115, 14);
		contentPane.add(lblLights);
		
		DefaultTableModel tableModelLights = new DefaultTableModel(new Object[][] {},
				new String[] { "Light ID",
				"State" }) {

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		
		tableLights = new JTable(tableModelLights);
		JScrollPane paneLights = new JScrollPane(tableLights);
		paneLights.setBounds(10, 36, 414, 75);
		contentPane.add(paneLights);
		
		DefaultTableModel tableModelWindows = new DefaultTableModel(new Object[][] {},
				new String[] { "Windows ID",
				"State" }) {

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		tableWindows = new JTable(tableModelWindows);
		JScrollPane paneWindows = new JScrollPane(tableWindows);
		paneWindows.setBounds(10, 147, 414, 75);
		contentPane.add(paneWindows);
		
		JLabel lblWindows = new JLabel("Windows");
		lblWindows.setBounds(10, 122, 115, 14);
		contentPane.add(lblWindows);
		
		DefaultTableModel tableModelDoors = new DefaultTableModel(new Object[][] {},
				new String[] { "Door ID",
				"State" }) {

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		tableDoors = new JTable(tableModelDoors);
		JScrollPane paneDoors = new JScrollPane(tableDoors);
		paneDoors.setBounds(10, 258, 414, 75);
		contentPane.add(paneDoors);
		
		JLabel lblDoors = new JLabel("Doors");
		lblDoors.setBounds(10, 233, 115, 14);
		contentPane.add(lblDoors);
		
		DefaultTableModel tableModelApp = new DefaultTableModel(new Object[][] {},
				new String[] { "Name",
				"State / Value" }) {

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		tableAppliances = new JTable(tableModelApp);
		JScrollPane paneAppliances = new JScrollPane(tableAppliances);
		paneAppliances.setBounds(10, 369, 414, 75);
		contentPane.add(paneAppliances);
		
		JLabel lblAppliances = new JLabel("Appliances");
		lblAppliances.setBounds(10, 344, 115, 14);
		contentPane.add(lblAppliances);
		
		JButton btnClose = new JButton("Close");
		btnClose.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				thisFrame.setVisible(false);
				thisFrame.dispose();
			}
		});
		btnClose.setBounds(171, 455, 89, 23);
		contentPane.add(btnClose);
		
		fillTables(d);
		
		setVisible(true);
	}
	
	public void windowClosed(WindowEvent e) {
		//System.out.println(e.getNewState());
	
	}

	
	public static void fillLights(Division d){
		DefaultTableModel model = ((DefaultTableModel) tableLights.getModel());
		for (int i = model.getRowCount() - 1; i >= 0; i--) {
			model.removeRow(i);
		}
		int id = 0;
		for(Integer i : d.getLightIntensity()){
			model.addRow(new Object[]{"Light " + id, i});
			id++;
		}
	}
	public static void fillDoors(Division d){
		DefaultTableModel model = ((DefaultTableModel) tableDoors.getModel());
		for (int i = model.getRowCount() - 1; i >= 0; i--) {
			model.removeRow(i);
		}
		int id = 0;
		for(Boolean i : d.getDoors()){
			if(i)
				model.addRow(new Object[]{"Door " + id, "Open"});
			else
				model.addRow(new Object[]{"Door " + id, "Closed"});
			id++;
		}
	}
	public static void fillWindows(Division d){
		DefaultTableModel model = ((DefaultTableModel) tableWindows.getModel());
		for (int i = model.getRowCount() - 1; i >= 0; i--) {
			model.removeRow(i);
		}
		int id = 0;
		for(Boolean i : d.getWindows()){
			if(i)
				model.addRow(new Object[]{"Window " + id, "Open"});
			else
				model.addRow(new Object[]{"Window" + id, "Closed"});
			id++;
		}
	}
	public static void fillAppliances(Division d){
		DefaultTableModel model = ((DefaultTableModel) tableAppliances.getModel());
		for (int i = model.getRowCount() - 1; i >= 0; i--) {
			model.removeRow(i);
		}
		for(Appliance app : d.getAppliances()){
			if(app.getState())
				model.addRow(new Object[]{app.getName(), "On"});
			else
				model.addRow(new Object[]{app.getName(), "Off"});
			
		}
	}
	public static void fillTables(Division d){
		fillAppliances(d);
		fillDoors(d);
		fillLights(d);
		fillWindows(d);
	}
}
