package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JLabel;
import javax.swing.JTable;

import jess.JessException;

import logic.Action;
import logic.houseManager;

public class ShowRule extends JFrame {

	private JPanel contentPane;
	private JScrollPane pane;
	private JTable table;
	private ShowRule thisFrame;

	

	/**
	 * Create the frame.
	 */
	public ShowRule() {
		thisFrame = this;
		
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblRules = new JLabel("Rules:");
		lblRules.setBounds(10, 11, 46, 14);
		contentPane.add(lblRules);
		
		DefaultTableModel tableModel = new DefaultTableModel(new Object[][] {},
				new String[] { "Action",
						"Division", "Consequence", "Trigger" }) {

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		
		table = new JTable(tableModel);
		table.setBounds(10, 36, 414, 215);
		fillTable();
		table.setEnabled(false);
		
		pane = new JScrollPane(table);
		pane.setBounds(10, 36, 414, 215);
		contentPane.add(pane);
		
		this.addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosed(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosing(WindowEvent arg0) {
				thisFrame.setVisible(false);
				thisFrame.dispose();
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

		});
		
		this.setVisible(true);
	}

	private void fillTable() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		
		try {
			for(Action a : houseManager.getManager().getActions()){
				model.addRow(new Object[] {a.getAction(), a.getDivision(), a.getConsequence(), a.getTrigger()});
			}
		} catch (JessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
