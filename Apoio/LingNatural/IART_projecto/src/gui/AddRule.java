package gui;

import javax.swing.JFrame;

import house.Appliance;

import house.Division;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import java.awt.Button;
import java.awt.Label;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import jess.JessException;

import logic.houseManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.JComboBox;

public class AddRule extends JFrame {

	public enum STATE {
		DIVISION, LIGHTSORAPPLIANCE, WHICH, FACTOR
	}

	private STATE state;
	private JPanel contentPane;
	private JTable table;
	private JFrame thisFrame;
	private JTextField txtNewAction;
	private String division;
	private String action;
	private String consequence;
	private int hour;
	private int min;
	private int value;
	private String type;
	private ButtonGroup group;
	private JSpinner spinnerValue;
	private JRadioButton rdbtnOn;
	private JRadioButton rdbtnOff;
	private JSpinner spinnerHour;
	private JSpinner spinnerMinutes;
	private JSpinner spinnerTemp;
	Label label_1;
	private JComboBox dayStage;
	
	/**
	 * Create the frame.
	 */
	public AddRule() {
		thisFrame = this;
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 350);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		DefaultTableModel tableModel = new DefaultTableModel(new Object[][] {},
				new String[] { "New column" }) {

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		
		table = new JTable();
		table.setModel(tableModel);
		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(10, 66, 564, 180);
		table.setTableHeader(null);
		contentPane.add(pane);

		Label label = new Label("Name:");
		label.setBounds(10, 10, 102, 22);
		contentPane.add(label);

		Button button = new Button("Cancel");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisFrame.setVisible(false);
				thisFrame.dispose();
			}
		});
		button.setBounds(504, 280, 70, 22);
		contentPane.add(button);

		label_1 = new Label("Choose a division:");
		label_1.setBounds(10, 38, 414, 22);
		contentPane.add(label_1);
		
		Button button_1 = new Button("Next");
		button_1.addMouseListener(new MouseAdapter() {
			private String stage;

			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row;
				switch (state) {
				case DIVISION:
					row = table.getSelectedRow();
					if (row < 0) {
						JOptionPane.showMessageDialog(thisFrame,
								"Plese select a row.");
						break;
					}
					division = (String) table.getValueAt(row, 0);
					state = STATE.LIGHTSORAPPLIANCE;
					label_1.setText("Choose the type of objects: ");
					updateTable();
					break;
				case LIGHTSORAPPLIANCE:
					
					row = table.getSelectedRow();
					if (row < 0) {
						JOptionPane.showMessageDialog(thisFrame,
								"Plese select a row.");
						break;
					}
					type = (String) table.getValueAt(row, 0);
					state = STATE.WHICH;
					label_1.setText("Which one?");
					updateTable();
					break;
				case WHICH:
					row = table.getSelectedRow();
					if (row < 0) {
						JOptionPane.showMessageDialog(thisFrame,
								"Plese select a row.");
						break;
					}
					consequence = (String) table.getValueAt(row, 0);
					state = STATE.FACTOR;
					label_1.setText("Choose between the factors and fill the values below the table");
					updateTable();
					break;
				case FACTOR:
					
					row = table.getSelectedRow();
					if (row < 0) {
						JOptionPane.showMessageDialog(thisFrame,
								"Plese select a row.");
						break;
					}
					String factor = (String) table.getValueAt(row, 0);
					action = txtNewAction.getText();
					switch (type) {
					case "lights":
						consequence = "lights_" + consequence + "_" + spinnerValue.getValue();
						break;
					case "appliances":
						if(rdbtnOn.isSelected()){
							consequence = "appliances_" + consequence + "_on";
						} else {
							consequence = "appliances_" + consequence + "_off";
						}
						
						break;
					case "windows":
						if(rdbtnOn.isSelected()){
							consequence = "windows_" + consequence + "_open";
						} else {
							consequence = "appliances_" + consequence + "_close";
						}
						break;
					case "doors":
						if(rdbtnOn.isSelected()){
							consequence = "doors_" + consequence + "_open";
						} else {
							consequence = "doors_" + consequence + "_close";
						}
						break;
					default:
						break;
					}
					switch (factor) {
					case "Time":
						hour = (Integer) spinnerHour.getValue();
						min = (Integer) spinnerMinutes.getValue();
						String rule = division + " " + action + " " + type + " " + consequence;
						try {
							if(houseManager.getManager().addRule(division, action, consequence, new String(hour + " : " + min))){
								houseManager.getManager().getJessM().addTimeFact(division, action, consequence, hour, min);
								JOptionPane.showMessageDialog(thisFrame,
										"Rule added successfully!");
							}
							
						} catch (JessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						break;
					case "Temperature":
						value = (Integer) spinnerTemp.getValue();
						try {
							if(houseManager.getManager().addRule(division, action, consequence, spinnerTemp.getValue().toString())){
								houseManager.getManager().getJessM().addTempFact(division, action, consequence, value);
								JOptionPane.showMessageDialog(thisFrame,
										"Rule added successfully!");
							}
							
						} catch (JessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case "Humidity":
						value = (Integer) spinnerTemp.getValue();
						try {
							if(houseManager.getManager().addRule(division, action, consequence, spinnerTemp.getValue().toString())){
								houseManager.getManager().getJessM().addHumidityFact(division, action, consequence, value);
								JOptionPane.showMessageDialog(thisFrame, "Rule added successfully!");
							}
							
						} catch (JessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case "House Empty":
						try {
							if(houseManager.getManager().addRule(division, action, consequence, "true")){
								houseManager.getManager().getJessM().addHouseEmptyFact(division, action, consequence, "true");
								JOptionPane.showMessageDialog(thisFrame, "Rule added successfully!");
							}
							
						} catch (JessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case "House Occupied":
						try {
							if(houseManager.getManager().addRule(division, action, consequence, "false")) {
								houseManager.getManager().getJessM().addHouseEmptyFact(division, action, consequence, "false");
								JOptionPane.showMessageDialog(thisFrame, "Rule added successfully!");
							}
						} catch (JessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case "Time and Temperature":
						hour = (Integer) spinnerHour.getValue();
						min = (Integer) spinnerMinutes.getValue();
						value = (Integer) spinnerTemp.getValue();
						try {
							if(houseManager.getManager().addRule(division, action, consequence, "false")){
								houseManager.getManager().getJessM().addTimeTemperatureFact(division, action, consequence, hour, min, value);
								JOptionPane.showMessageDialog(thisFrame,
										"Rule added successfully!");
							}
							
						} catch (JessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case "Temperature Difference":
						int balue = (Integer)spinnerTemp.getValue();
						try {
							if(houseManager.getManager().addRule(division, action, consequence, "false")){
								houseManager.getManager().getJessM().addTempDiffFact(division, action, consequence, balue);
								JOptionPane.showMessageDialog(thisFrame,
										"Rule added successfully!");
							}
							
						} catch (JessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case "Day Stage":
						stage = (String) dayStage.getSelectedItem();
						try {
							if(houseManager.getManager().addRule(division, action, consequence, "false")){	
								houseManager.getManager().getJessM().addDayStageFact(division, action, consequence, stage);
								JOptionPane.showMessageDialog(thisFrame,
										"Rule added successfully!");
							}
							
						} catch (JessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
					case "Temperature Difference and Day Stage":
						value = (Integer) spinnerTemp.getValue();
						stage = (String) dayStage.getSelectedItem();
						System.out.println("ADDED");
						try {
							if(houseManager.getManager().addRule(division, action, consequence, "false")){
								houseManager.getManager().getJessM().addTempDiffDayStageFact(division, action, consequence, value, stage);
								JOptionPane.showMessageDialog(thisFrame,
										"Rule added successfully!");	
							}
							
						} catch (JessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						break;
						default:
						break;
					}
					thisFrame.setVisible(false);
					thisFrame.dispose();
					break;
				default:
					break;
				}
			}
		});
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		button_1.setBounds(504, 252, 70, 22);
		contentPane.add(button_1);

		txtNewAction = new JTextField();
		txtNewAction.setText("New action");
		txtNewAction.setBounds(118, 12, 150, 20);
		contentPane.add(txtNewAction);
		txtNewAction.setColumns(10);

		JLabel lblHour = new JLabel("Time:");
		lblHour.setBounds(10, 280, 67, 14);
		contentPane.add(lblHour);

		spinnerHour = new JSpinner(new SpinnerNumberModel(0, 0, 23, 1));
		spinnerHour.setBounds(87, 274, 38, 20);
		contentPane.add(spinnerHour);

		spinnerMinutes = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
		spinnerMinutes.setBounds(128, 274, 38, 20);
		contentPane.add(spinnerMinutes);

		JLabel lblTemp = new JLabel("Factor value:");
		lblTemp.setBounds(196, 280, 98, 14);
		contentPane.add(lblTemp);

		spinnerTemp = new JSpinner(new SpinnerNumberModel(25, -10, 40, 1));
		spinnerTemp.setBounds(281, 282, 38, 20);
		contentPane.add(spinnerTemp);

		JLabel lblValue = new JLabel("Object value:");
		lblValue.setBounds(196, 252, 98, 14);
		contentPane.add(lblValue);

		spinnerValue = new JSpinner(new SpinnerNumberModel(0, 0, 100, 1));
		spinnerValue.setBounds(281, 254, 38, 20);
		contentPane.add(spinnerValue);

		JLabel lblState = new JLabel("Object state:");
		lblState.setBounds(10, 252, 75, 14);
		contentPane.add(lblState);

		group = new ButtonGroup();

		rdbtnOn = new JRadioButton("On");
		rdbtnOn.setSelected(true);
		group.add(rdbtnOn);
		rdbtnOn.setBounds(87, 248, 43, 23);
		contentPane.add(rdbtnOn);

		rdbtnOff = new JRadioButton("Off");
		group.add(rdbtnOff);
		rdbtnOff.setBounds(128, 248, 43, 23);
		contentPane.add(rdbtnOff);
		
		JLabel lblStageOfDay = new JLabel("Day Stage:");
		lblStageOfDay.setBounds(365, 257, 77, 14);
		contentPane.add(lblStageOfDay);
		
		dayStage = new JComboBox();
		dayStage.setBounds(352, 274, 102, 20);
		dayStage.addItem("NIGHT");
		dayStage.addItem("MORNING");
		dayStage.addItem("MIDDAY");
		dayStage.addItem("AFTERNOON");
		dayStage.addItem("EVENING");
		contentPane.add(dayStage);

		state = STATE.DIVISION;
		updateTable();

		this.addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosed(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosing(WindowEvent arg0) {
				thisFrame.setVisible(false);
				thisFrame.dispose();
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

		});
		
		this.setVisible(true);
	}

	private void updateTable() {
		int line = 0;
		DefaultTableModel model = ((DefaultTableModel) table.getModel());
		for (int i = model.getRowCount() - 1; i >= 0; i--) {
			model.removeRow(i);
		}
		switch (state) {
		case DIVISION:
			try {
				for (Division d : houseManager.getManager().getDivisions()) {
					model.addRow(new Object[] { d.getName() });
					//model.
				}
			} catch (JessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case LIGHTSORAPPLIANCE:
			model.addRow(new Object[] { "lights" });
			model.addRow(new Object[] { "appliances" });
			model.addRow(new Object[] { "windows" });
			model.addRow(new Object[] { "doors" });
			break;
		case WHICH:
			Division dv = null;
			try {
				for (Division d : houseManager.getManager().getDivisions()) {
					if (division.equals(d.getName())) {
						dv = d;
						break;
					}
				}
			} catch (JessException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			model.addRow(new Object[] { "all" });
			switch (type) {
			case "lights":
				for (int i = 0; i < dv.getLightIntensity().size(); i++) {
					model.addRow(new Object[] { "Light " + i });
				}
				break;
			case "windows":
				for (int i = 0; i < dv.getWindows().size(); i++) {
					model.addRow(new Object[] { "Window " + i });
				}
				break;
			case "doors":
				for (int i = 0; i < dv.getWindows().size(); i++) {
					model.addRow(new Object[] { "Door " + i });
				}
				break;
			case "appliances":
				for (Appliance a : dv.getAppliances()) {
					model.addRow(new Object[] { a.getName() });
				}
				break;
			default:
				break;
			}
			break;
		case FACTOR:
			model.addRow(new Object[] { "Time" });
			model.addRow(new Object[] { "Temperature" });
			model.addRow(new Object[] { "Humidity" });
			model.addRow(new Object[] { "House Empty" });
			model.addRow(new Object[] { "House Occupied" });
			model.addRow(new Object[] { "Temperature Difference" });
			model.addRow(new Object[] { "Time and Temperature" });
			model.addRow(new Object[] { "Day Stage" });
			model.addRow(new Object[] { "Temperature Difference and Day Stage" });
			break;
		default:
			break;
		}
		//table.setEnabled(false);
	}
}