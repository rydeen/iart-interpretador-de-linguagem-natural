package gui;

import house.Division;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import jess.JessException;
import logic.Action;
import logic.houseManager;

public class DeleteDivision extends JFrame {
	private JTable table;
	private JPanel contentPane;
	private DeleteDivision thisFrame;
	
	/**
	 * Create the frame.
	 */
	public DeleteDivision() {
		thisFrame = this;
		
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblChooseARule = new JLabel("Choose a Rule:");
		lblChooseARule.setBounds(10, 11, 101, 14);
		contentPane.add(lblChooseARule);
		
		JButton btnNext = new JButton("Delete");
		btnNext.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row = table.getSelectedRow();
				if (row < 0) {
					JOptionPane.showMessageDialog(thisFrame,
							"Plese select a row.");
					return;
				}
				String action = (String) table.getValueAt(row, 0);
				try {
					houseManager.getManager().removeDivision(row);
				} catch (JessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				JOptionPane.showMessageDialog(thisFrame,
						action + " removed.");
				fillTable();
			}
		});
		btnNext.setBounds(335, 228, 89, 23);
		contentPane.add(btnNext);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				thisFrame.setVisible(false);
				thisFrame.dispose();
			}
		});
		btnCancel.setBounds(10, 228, 89, 23);
		contentPane.add(btnCancel);
		
		DefaultTableModel tableModel = new DefaultTableModel(new Object[][] {},
				new String[] { "Division" }) {

		    @Override
		    public boolean isCellEditable(int row, int column) {
		       return false;
		    }
		};
		
		table = new JTable(tableModel);
		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(10, 36, 414, 181);
		contentPane.add(pane);
		
		fillTable();
		
		this.addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosed(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowClosing(WindowEvent e) {
				thisFrame.setVisible(false);
				thisFrame.dispose();
				
			}

			@Override
			public void windowDeactivated(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowDeiconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowIconified(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void windowOpened(WindowEvent e) {
				// TODO Auto-generated method stub
				
			}

			

		});
		
		this.setVisible(true);
	}

	private void fillTable() {
		DefaultTableModel model = ((DefaultTableModel) table.getModel());
		for (int i = model.getRowCount() - 1; i >= 0; i--) {
			model.removeRow(i);
		}
		try {
			for(Division a : houseManager.getManager().getDivisions()){
				model.addRow(new Object[]{a.getName()});
			}
		} catch (JessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
