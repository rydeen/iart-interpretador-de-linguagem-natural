package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Label;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import jess.JessException;

import logic.houseManager;

public class AddAppliance extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private AddAppliance thisFrame;


	/**
	 * Create the frame.
	 * @param string 
	 */
	public AddAppliance(String string) {
		thisFrame = this;
		thisFrame.setName(string);
		
		setBounds(100, 100, 223, 102);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Label label = new Label("Name: ");
		label.setBounds(10, 10, 62, 22);
		contentPane.add(label);
		
		textField = new JTextField();
		textField.setBounds(77, 10, 121, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnSave = new JButton("Save");
		btnSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(textField.getText().equals("")){
					JOptionPane.showMessageDialog(thisFrame, "Please specify the name of the appliance first.");
					return;
				}
				try {
					if(!houseManager.getManager().addApplianceToDivision(thisFrame.getName(), thisFrame.textField.getText())){
						textField.setText("");
						JOptionPane.showMessageDialog(thisFrame, "Appliance already added.");
						return;
					} else{
						textField.setText("");
					}
					
				} catch (JessException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnSave.setBounds(109, 38, 89, 23);
		contentPane.add(btnSave);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				thisFrame.setVisible(false);
				thisFrame.dispose();
			}
		});
		btnCancel.setBounds(10, 38, 89, 23);
		contentPane.add(btnCancel);
		
		this.addWindowListener(new WindowListener() {

			@Override
			public void windowActivated(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosed(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowClosing(WindowEvent arg0) {
				thisFrame.setVisible(false);
				thisFrame.dispose();
			}

			@Override
			public void windowDeactivated(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowDeiconified(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowIconified(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void windowOpened(WindowEvent arg0) {
				// TODO Auto-generated method stub

			}

		});
		
		this.setVisible(true);
	}
}
