package logic;

import gui.ShowDivision;
import gui.mainFrame;
import house.Division;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import jess.JessException;

import logic.StageOfTheDay;

public class houseManager extends Thread {
	public static int KITCHEN = 0;
	public static int BEDROOM = 1;
	public static int WC = 2;
	public static int LIVINGROOM= 3;
	

	private StageOfTheDay stageOfTheDay = StageOfTheDay.NIGHT;
	
	public static final int NIGHT_TO_MORNING = 6;
	public static final int MORNING_TO_MIDDAY = 12;
	public static final int MIDDAY_TO_AFTERNOON = 16;
	public static final int AFTERNOON_TO_EVENING = 20;
	public static final int EVENING_TO_NIGHT = 23;
	
	private static final double TEMPERATURE_VARIATION = 0.02;
	private static final int HUMIDITY_VARIATION_PER_HOUR = 1;
	private static final double TEMPERATURE_APPLIANCE_VARIATION = 0.3;
	
	private static jessManager jessM;
	private int minutes;
	private int hours;
	private ArrayList<Action> actions = new ArrayList<Action>();
	private ArrayList<Division> divisions = new ArrayList<Division>();
	private boolean houseEmpty;
	
	static private houseManager manager = null;
	
	static public houseManager getManager() throws JessException{
		if(manager == null)
			manager = new houseManager();
		return manager;
	}
	
	public void saveActions(){
		//Serializar e gravar actions para um ficheiro
	}
	
	public void loadActions(){
		//Des-serializar e carregar actions do ficheiro		
	}
	
	public boolean addDivisions() throws JessException{
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the name of the division: (quit to stop entering actions) ");
		String name = sc.nextLine();
		if(name.equals("quit")){
			return false;
		}else{
			System.out.println("Number of Windows: ");
			int now = Integer.parseInt(sc.nextLine());
			
			System.out.println("Number of doors: ");
			int nod = Integer.parseInt(sc.nextLine());
			
			System.out.println("Number of lights: ");
			int nol = Integer.parseInt(sc.nextLine());
			
			this.getDivisions().add(new Division(now,nod,nol,name));
			
			while(addRulesToDivision(name));
			
			return true;
			
		}
	}
	
	private boolean addRulesToDivision(String division) throws JessException {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the action: (quit to stop entering actions) ");
		String action = sc.nextLine();
		if(action.equals("quit")){
			return false;
		}else{
			System.out.println("Enter the consequence of the action:  ");
			String consequence = sc.nextLine();
			
			System.out.println("Is the action depending on time or on a value? (T/V)");
			Character op = sc.nextLine().charAt(0);
			op = Character.toUpperCase(op);
			if( op == 'T'){
				System.out.println("Hour: ");
				int hour = Integer.parseInt(sc.nextLine());
				
				System.out.println("Minute: ");
				int min = Integer.parseInt(sc.nextLine());
				getJessM().addTimeFact(division, action, consequence, hour, min);
			}else if(op == 'V'){
				System.out.println("Temperature, Luminosity, or Humidity? (T/L/H)");
				Character op2 = sc.nextLine().charAt(0);
				System.out.println("Value: ");
				int value = Integer.parseInt(sc.nextLine());
				
				op2 = Character.toUpperCase(op2);
				switch (op2){
				case 'T' :
					getJessM().addTempFact(division, action, consequence, value);
					break;
				case 'L': 
					getJessM().addLuminosityFact(division, action, consequence, value);
					break;
				case 'H': 
					getJessM().addHumidityFact(division, action, consequence, value);
					break;
				
				}
			}
			
			return true;
			
		}
		
	}

	public boolean addRule() throws JessException{
		Scanner sc=new Scanner(System.in);
		System.out.println("What do you want to do? (quit to stop entering actions) ");
		String action=sc.nextLine();
		if(action.equals("quit")){
			return false;
		}else{
			System.out.println("Hour of that event: ");
			int hour = Integer.parseInt(sc.nextLine());
			
			System.out.println("Minute of that event: ");
			int minute = Integer.parseInt(sc.nextLine());
			
			//getJessM().addRule(action,hour,minute);
			
			return true;
			
		}
		
	}
	public houseManager() throws JessException{
		manager = this;
		
		setJessM(new jessManager());
		loadActions();
		this.hours = jessM.getHours();
		this.minutes = jessM.getMinutes();
	}
	
	public static void main(String[] args) throws JessException {
		houseManager x = new houseManager();
		
	}

	public void notify(String division, String action, String consequence){
		System.out.println("[HM] Received message.");
		System.out.println("\tDivision : " + division);
		System.out.println("\tAction : " + action);
		System.out.println("\tConsequence : " + consequence);
		
		String[] split = consequence.split("_");
		String type = split[0];
		String object = split[1];
		String acao = split[2];
		Division target = null;
		for(Division d : getDivisions()){
			if(d.getName().equals(division)){
				target = d;
				break;
			}
		}
		
		switch(type){
		case "lights":
			System.out.println("modyfying lights");
			target.modifyLights(object, acao);
			break;
		case "appliances":
			System.out.println("modifying appliances");
			target.modifyAppliances(object, acao);
			break;
		case "doors":
			System.out.println("modifying doors");
			target.modifyDoors(object, acao);
			break;
			
		case "windows":
			System.out.println("modifying windows");
			target.modifyWindows(object, acao);
			break;
		default:
			System.out.println("Type not recognized");
			break;
		}
		if(ShowDivision.showing)
			ShowDivision.fillTables(target);
	}
	
	public void parseJessMessage(String message){
		String[] split = message.split(" ");
		
		String object = split[0];
		String action = split[1];
		
		int howMuch = -1;
		
		if(action.equals("increase") || action.equals("decrease"))
			howMuch = Integer.parseInt(split[3]);
	
		System.out.println("Object: " + object);
		System.out.println("Action: " + action);
		System.out.println("Value: " + howMuch);
		
		//gui.changeStatus(object, action, howMuch)
		//na fun��o, if(howMuch == -1) ignorar o howMuch
	}
	
	public void update() throws JessException{
		if(jessM.getMinutes() == 0){
			//REFRESH STAGE OF THE DAY
			switch(jessM.getHours()){
			case NIGHT_TO_MORNING:
				this.setStageOfTheDay(StageOfTheDay.MORNING);
				break;
			case MORNING_TO_MIDDAY:
				this.setStageOfTheDay(StageOfTheDay.MIDDAY);
				break;
			case MIDDAY_TO_AFTERNOON :
				this.setStageOfTheDay(StageOfTheDay.AFTERNOON);
				break;
			case AFTERNOON_TO_EVENING :
				this.setStageOfTheDay(StageOfTheDay.EVENING);
				break;
			case EVENING_TO_NIGHT :
				this.setStageOfTheDay(StageOfTheDay.NIGHT);
				break;
			}
			
			//UPDATE STAGE OF THE DAY ON JESS
			jessM.updateStage(this.stageOfTheDay);
			
			//MODIFY AMBIENT VALUES DEPENDING ON THE STAGE OF THE DAY, ONCE PER HOUR
			switch(this.getStageOfTheDay()){
			case MORNING: 
				this.jessM.updateLuminosity(75);
				break;
			case MIDDAY:
				this.jessM.updateHumidity(jessM.getHumidity() - this.HUMIDITY_VARIATION_PER_HOUR); // decrease humidity
				this.jessM.updateLuminosity(100);
				break;
			case AFTERNOON:
				this.jessM.updateHumidity(jessM.getHumidity() - this.HUMIDITY_VARIATION_PER_HOUR); // decrease humidity
				this.jessM.updateLuminosity(100);
				break;
			case EVENING:
				this.jessM.updateHumidity(jessM.getHumidity() + this.HUMIDITY_VARIATION_PER_HOUR); // increase humidity
				this.jessM.updateLuminosity(45);
				break;
			case NIGHT:
				this.jessM.updateHumidity(jessM.getHumidity() + this.HUMIDITY_VARIATION_PER_HOUR); // increase humidity
				this.jessM.updateLuminosity(0);
				break;
			}
			
		}
		
		switch(this.getStageOfTheDay()){
		case MORNING: 
			this.jessM.updateTemperature(jessM.getCurrentTemp() + this.TEMPERATURE_VARIATION); // increase temperature
			break;
		case MIDDAY:
			this.jessM.updateTemperature(jessM.getCurrentTemp() + this.TEMPERATURE_VARIATION);
			break;
		case EVENING:
			this.jessM.updateTemperature(jessM.getCurrentTemp() - this.TEMPERATURE_VARIATION); // decrease temperature
			break;
		case NIGHT:
			this.jessM.updateTemperature(jessM.getCurrentTemp() - this.TEMPERATURE_VARIATION); // decrease temperature
			break;
		default: break;
		}
		
		double tempIncrease = jessM.getInsideTemperature();
		double humidityIncrease = jessM.getHumidity();
		
		for(int i = 0; i < divisions.size(); i++){
			for(int j = 0; j < divisions.get(i).getAppliances().size(); j++){
				if(divisions.get(i).getAppliances().get(j).getName().equals("A/C") && divisions.get(i).getAppliances().get(j).getState()){
					tempIncrease -= TEMPERATURE_APPLIANCE_VARIATION;
				}
				if(divisions.get(i).getAppliances().get(j).getName().equals("Heater") && divisions.get(i).getAppliances().get(j).getState()){
					tempIncrease += TEMPERATURE_APPLIANCE_VARIATION;
				}
				if(divisions.get(i).getAppliances().get(j).getName().equals("Dehumidifier") && divisions.get(i).getAppliances().get(j).getState()){
					humidityIncrease += TEMPERATURE_APPLIANCE_VARIATION;
				}
			}
		}
		
		double out = jessM.getCurrentTemp();
		double in = jessM.getInsideTemperature();
		
		
		if(out != in){
			if(out > in){
				tempIncrease += (out-in)/10;
			} else
				tempIncrease -= (in-out)/10;
		}
		jessM.updateTemperatureIndoor(tempIncrease);
		jessM.updateHumidity(humidityIncrease);
		
	}

	public void updateTemperature(double value) throws JessException {
		getJessM().updateTemperature(value);
		
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public static jessManager getJessM() {
		return jessM;
	}

	public static void setJessM(jessManager jessM) {
		houseManager.jessM = jessM;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		if(hours == 24)
			this.hours = 0;
		else
			this.hours = hours;
	}

	public ArrayList<Division> getDivisions() {
		return divisions;
	}

	public void setDivisions(ArrayList<Division> divisions) {
		this.divisions = divisions;
	}

	public boolean addRule(String division, String action, String consequence, String string) {
		for(Action a : actions)
			if(a.getAction().equals(action)){
				mainFrame.showAlert("Error while adding rule. There is already a rule defined with that name!");
				return false;
			}
		
		actions.add(new Action(division, action, consequence, string));
		return true;
		
	}
	
	public int getRuleByAction(String action){
		for(int i = 0; i < actions.size(); i++){
			if(actions.get(i).getAction().equals(action))
				return i;
		}
		return -1;
	}
	
	public int getNumberOfFacts(){
		return jessM.getNumberOfFacts();
	}
	
	public ArrayList<Action> getActions(){
		return actions;
	}
	
	public void removeFact(int id){
		try {
			jessM.removeFact(jessM.getNumberOfFacts() + id);
			actions.remove(id);
		} catch (JessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean addDivision(String name, int windows, int doors, int lights){
		for(Division d : divisions){
			if(d.getName().equals(name))
				return false;
		}
		
		divisions.add(new Division(windows, doors, lights, name));
		return true;
		
	}

	public boolean addApplianceToDivision(String name, String appliance) {
		for(int i = 0; i < divisions.size(); i++){
			if(divisions.get(i).getName().equals(name)){
				if(divisions.get(i).hasAppliance(appliance))
					return false;
				else{
					divisions.get(i).addAppliance(appliance);
					return true;
				}
			}
		}
		return false;
		
	}

	public void removeDivision(int row) {
		this.divisions.remove(row);
		
	}
	
	public Division getDivisionByID(int id){
		return divisions.get(id);
		
	}

	public void setHouseEmpty() throws JessException {
		this.houseEmpty = true;
		jessM.updateHouseEmpty(true);
		
	}

	public void setHouseOccupied() throws JessException {
		this.houseEmpty = true;
		jessM.updateHouseEmpty(false);
		
	}

	public StageOfTheDay getStageOfTheDay() {
		return stageOfTheDay;
	}

	public void setStageOfTheDay(StageOfTheDay stageOfTheDay) {
		this.stageOfTheDay = stageOfTheDay;
	}
	
}
