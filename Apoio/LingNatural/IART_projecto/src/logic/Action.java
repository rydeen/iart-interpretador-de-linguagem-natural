package logic;

public class Action {
	private String action;
	private String consequence;
	private String trigger;
	private String division;
	
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getConsequence() {
		return consequence;
	}
	public void setConsequence(String consequence) {
		this.consequence = consequence;
	}
	public String getTrigger() {
		return trigger;
	}
	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}
	
	public Action(String division, String action, String consequence, String trigger){
		this.action = action;
		this.consequence = consequence;
		this.trigger = trigger;
		this.division = division;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	
	public String showAction(){
		String returning = "";
		returning += "Action: " + action + "\n";
		returning += "Consequence: " + consequence + "\n";
		returning += "Trigger: " + trigger + "\n\n";
		
		return returning;
	}

}
