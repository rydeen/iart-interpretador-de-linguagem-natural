package logic;

public enum StageOfTheDay {
	NIGHT, MORNING, MIDDAY, AFTERNOON, EVENING
}
