package logic;
import gui.mainFrame;

import java.util.Iterator;

import jess.Deftemplate;
import jess.JessEvent;
import jess.JessException;
import jess.Rete;
import jess.Value;
public class jessManager {
	
	
	private Rete jessObj;
	private int hours = 5;
	private int minutes = 55;
	Value x = null;
	private Double outsideTemperature = 15.0;
	private boolean houseEmpty = false;
	private int numberStaticOfFacts = 0;
	private int outsideLuminosity = 0;
	private double outsideHumidity = 50;
	
	private Double insideTemperature = 15.0;
	private int temperatureDiff = 0;
	
	private int temperature = 15;
	
	
	public void update() throws JessException{
		this.setMinutes(this.getMinutes() + 1);
		if(getMinutes() == 60){
			this.setHours(this.getHours() + 1);
			this.setMinutes(0);
		}
		try {
			jessObj.eval("(modify ?ct (hour "+ this.getHours() +") (min "+ this.getMinutes() +") )");
		} catch (JessException e1) {
			e1.printStackTrace();
		}
		this.run();
		
	}
	
	public void updateTemperature(double tempIncrease) throws JessException{
		setCurrentTemp(tempIncrease);
		
		Double d = outsideTemperature - insideTemperature;
		if(temperatureDiff != d.intValue()){
			temperatureDiff = d.intValue();
			try{
				jessObj.eval("(modify ?tempDifference (value " + temperatureDiff + "))");
			} catch(JessException e){
				e.printStackTrace();
				System.out.println("Temperature difference sensor has an error.");
			}
		}
		((mainFrame) mainFrame.getFrame()).updateTemperature((double)this.outsideTemperature);
	}
	
	public void updateLuminosity(int value) throws JessException{
		this.setLuminosity(value);
		jessObj.eval("(modify ?luminosity (value "+ this.getCurrentTemp() +") )");
		((mainFrame) mainFrame.getFrame()).updateLuminosity(this.outsideLuminosity);
	}
	
	public void updateHumidity(double humidityIncrease) throws JessException{
		this.setHumidity(humidityIncrease);
		jessObj.eval("(modify ?humidity (value "+ this.getCurrentTemp() +") )");
		((mainFrame) mainFrame.getFrame()).updateHumidity(this.outsideHumidity);
	}
	
	public void updateHouseEmpty(boolean value) throws JessException{
		this.houseEmpty = value;
		if(value)
			jessObj.eval("(modify ?houseEmpty (empty true))");
		else
			jessObj.eval("(modify ?houseEmpty (empty false))");
		
	}
	
	public void updateStage(StageOfTheDay stage) throws JessException{
		jessObj.eval("(modify ?stage (stage "+stage.toString()+"))");
		((mainFrame)mainFrame.getFrame()).updateStage(stage.toString());
	}
	
	
	public jessManager(){
		jessObj =  new Rete();

		try {
			jessObj.batch("./controlFile.clp");
			
			jessObj.eval("(bind ?ct (assert (current-time (hour 0) (min 0))))");
			numberStaticOfFacts++;
			jessObj.eval("(bind ?temp (assert (current-temp (value 12))))");
			numberStaticOfFacts++;
			jessObj.eval("(bind ?luminosity (assert (current-luminosity (value 0))))");
			numberStaticOfFacts++;
			jessObj.eval("(bind ?humidity (assert (current-humidity (value 60))))");
			numberStaticOfFacts++;
			jessObj.eval("(bind ?houseEmpty (assert (house-occupied (empty false))))");
			numberStaticOfFacts++;
			jessObj.eval("(bind ?comp-true (assert (composed-true (value true))))");
			numberStaticOfFacts++;
			jessObj.eval("(bind ?comp-false (assert (composed-false (value false))))");
			numberStaticOfFacts++;
			jessObj.eval("(bind ?tempDifference (assert (difference (value 0))))");
			//jessObj.eval("(bind ?humidity-diff (assert (difference (value 5))))");
			//jessObj.eval("(bind ?luminosity-diff (assert (difference (value 5))))");
			numberStaticOfFacts++;
			//jessObj.eval("(bind ?temp-interior (assert (current-temp (value 25))))");
			//numberStaticOfFacts++;
			jessObj.eval("(bind ?stage (assert (stageOfTheDay (stage "+ houseManager.getManager().getStageOfTheDay().toString() + "))))");
			numberStaticOfFacts++;
			
			/*
			addTimeTemperatureFact("main room", "new action" , "devia_fazer_cenas aqui ", 0, 5,20);
			addTimeFact("main room", "new action" , "devia_fazer_mais milh�es de cenas aqui ", 0, 5);
			addTimeFact("main room", "new action 2" , "devia_fazer_menos milh�es de cenas aqui ", 0, 5);
			*/
			//System.out.println("Done");
			
		} catch (JessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void run(){
		try {
			jessObj.run();
		} catch (JessException e) {}
		
	}
	public void addTimeFact(String division, String action, String consequence,
			int hour, int min) throws JessException {
		jessObj.eval("(assert (scheduled-action-with-consequence (hour "+hour+") (min "+min+") (action \"" + action + "\") (consequence \"" + consequence + "\")  (division \"" + division + "\") (composed false) ))");
		
	}
	
	public void addHouseEmptyFact(String division, String action, String consequence,
			String empty) throws JessException {
		jessObj.eval("(assert (action-on-house-empty (empty "+empty+") (action \"" + action + "\") (consequence \"" + consequence + "\")  (division \"" + division + "\") (composed false) ))");
		
	}
	
	public void addTempFact(String division, String action, String consequence,
			int value) throws JessException {
		//double balue = value;
		jessObj.eval("(assert (action-on-value-temp (value "+value +") (action \""+ action + "\") (consequence \""+ consequence + "\") (division \""+ division+"\") (composed false)))");
	}
	public void addLuminosityFact(String division, String action, String consequence,
			int value) throws JessException {
		jessObj.eval("(assert (action-on-value-luminosity (value "+value+") (action \""+ action + "\") (consequence \""+ consequence + "\") (division \""+ division+"\") (composed false)))");
	}
	public void addHumidityFact(String division, String action, String consequence,
			int value) throws JessException {
		jessObj.eval("(assert (action-on-value-humidity (value "+value+") (action \""+ action + "\") (consequence \""+ consequence + "\") (division \""+ division+"\") (composed false)))");
	}

	public int getNumberOfFacts() {
		return numberStaticOfFacts;
	}
	
	public void removeFact(int id) throws JessException{
		jessObj.eval("(retract (fact-id "+ (id-1) +"))");
		numberStaticOfFacts++;
	}
	
	/* COMPOSED */
	public void addTimeTemperatureFact(String division, String action, String consequence, int hour, int min, int temp) throws JessException{
		jessObj.eval("(assert (scheduled-action-with-consequence (hour "+hour+") (min "+min+") (action \"" + action + "\") (consequence \"" + consequence + "\")  (division \"" + division + "\") (composed true) ))");
		jessObj.eval("(assert (action-on-value-temp (value "+temp+") (action \""+ action + "\") (consequence \""+ consequence + "\") (division \""+ division+"\") (composed true)))");
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		if(hours != 24)
			this.hours = hours;
		else
			this.hours = 0;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public double getCurrentTemp() {
		return outsideTemperature;
	}

	public void setCurrentTemp(double tempIncrease) {
		this.outsideTemperature = tempIncrease;
	}

	public int getLuminosity() {
		return outsideLuminosity;
	}

	public void setLuminosity(int luminosity) {
		this.outsideLuminosity = luminosity;
	}

	public double getHumidity() {
		return outsideHumidity;
	}

	public void setHumidity(double humidityIncrease) {
		this.outsideHumidity = humidityIncrease;
	}
	public void addTempDiffFact(String division, String action,
			String consequence, int balue) throws JessException{
		
		jessObj.eval("(assert (temperature-difference-action (value "+balue+") (action \""+ action + "\") (consequence \""+ consequence + "\") (division \""+ division+"\") (composed false)))");
		
	}

	public void addDayStageFact(String division, String action,
			String consequence, String stage) throws JessException {
		jessObj.eval("(assert (day-state-change-action (stage "+stage+") (action \""+ action + "\") (consequence \""+ consequence + "\") (division \""+ division+"\") (composed false)))");
		
	}

	public void addTempDiffDayStageFact(String division, String action,
			String consequence, int value, String stage) throws JessException {
		jessObj.eval("(assert (day-state-change-action (stage "+stage+") (action \""+ action + "\") (consequence \""+ consequence + "\") (division \""+ division+"\") (composed true)))");
		jessObj.eval("(assert (temperature-difference-action (value "+value+") (action \""+ action + "\") (consequence \""+ consequence + "\") (division \""+ division+"\") (composed true)))");
	}

	public double getInsideTemperature() {
		return insideTemperature;
	}

	public void setInsideTemperature(double insideTemperature) throws JessException {
		
		this.insideTemperature = insideTemperature;
		if(temperature != this.insideTemperature.intValue()){
			temperature = this.insideTemperature.intValue();
				try{
					jessObj.eval("(modify ?temp (value "+ temperature +") )");
				} catch(JessException e){
					System.out.println("Temperature sensor has an error.");
				}
			}
		
		Double d = outsideTemperature - insideTemperature;
		if(temperatureDiff != d.intValue()){
			temperatureDiff = d.intValue();
			try{
				jessObj.eval("(modify ?tempDifference (value " + temperatureDiff + "))");
			} catch(JessException e){
				e.printStackTrace();
				System.out.println("Temperature difference sensor has an error.");
			}
		}
		
		this.run();
	}

	public void updateTemperatureIndoor(double tempIncrease) {
		try {
			setInsideTemperature(tempIncrease);
		} catch (JessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*jessObj.eval("(modify ?temp (value "+ this.getCurrentTemp() +") )");
		jessObj.eval("(modify ?temp-diff (value " + (this.getCurrentTemp() - getInsideTemperature()) + "))");*/
		((mainFrame) mainFrame.getFrame()).updateInsideTemperature((double)this.insideTemperature);
		
	}
	
	
}
