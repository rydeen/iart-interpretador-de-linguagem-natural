(import logic.houseManager) 
(bind ?b (houseManager.getManager))

/* --------------- TEMPLATES  --------------- */

(deftemplate current-time (slot hour) (slot min))
(deftemplate house-occupied (slot empty)) /*empty = true || empty = false*/
(deftemplate current-temp (slot value))
(deftemplate current-luminosity (slot value))
(deftemplate current-humidity (slot value))
(deftemplate difference (slot value))
(deftemplate stageOfTheDay (slot stage))

(deftemplate composed-true (slot value))
(deftemplate composed-false (slot value))


(deftemplate luminosity-exterior (slot value))
(deftemplate temperature-exterior (slot value))
(deftemplate humidity-exterior (slot value))


(deftemplate scheduled-action-with-consequence
   (slot hour)
   (slot min)
   (slot action)
   (slot consequence)
   (slot division)
   (slot composed)
)

(deftemplate temperature-difference-action
   (slot value)
   (slot action)
   (slot consequence)
   (slot division)
   (slot composed)
)

(deftemplate day-state-change-action
   (slot stage)
   (slot action)
   (slot consequence)
   (slot division)
   (slot composed)
)


(deftemplate action-on-value-temp
   (slot value)
   (slot action)
   (slot consequence)
   (slot division)
   (slot composed)
)

(deftemplate action-on-house-empty
   (slot empty)
   (slot action)
   (slot consequence)
   (slot division)
   (slot composed)
)

(deftemplate action-on-value-luminosity
   (slot value)
   (slot action)
   (slot consequence)
   (slot division)
   (slot composed)
)

(deftemplate action-on-value-humidity
   (slot value)
   (slot action)
   (slot consequence)
   (slot division)
   (slot composed)
)


/* --------------- SIMPLE RULES  --------------- */ 			
(defrule run-scheduled-actions-with-consequence
   (scheduled-action-with-consequence (hour ?h) (min ?m) (action ?a) (consequence ?c) (division ?d) (composed ?cmp))
   (current-time (hour ?h) (min ?m) )
   (composed-false (value ?cmp))
   =>
   (?b notify ?d ?a ?c )
)
   
(defrule run-action-on-value-temp
	(action-on-value-temp (value ?v) (action ?a) (consequence ?c) (division ?d) (composed ?cmp))
	(current-temp (value ?v))
    (composed-false (value ?cmp))
	=>
	(?b notify ?d ?a ?c))

(defrule run-action-on-value-humidity
	(action-on-value-humidity (value ?v) (action ?a) (consequence ?c) (division ?d) (composed ?cmp))
	(current-luminosity (value ?v))
    (composed-false (value ?cmp))
	=>
	(?b notify ?d ?a ?c))

(defrule run-action-on-value-luminosity
	(action-on-value-luminosity (value ?v) (action ?a) (consequence ?c) (division ?d) (composed ?cmp))
	(current-humidity (value ?v))
    (composed-false (value ?cmp))
	=>
	(?b notify ?d ?a ?c))

(defrule run-action-on-house-empty
   (action-on-house-empty (empty ?e) (action ?a) (consequence ?c) (division ?d) (composed ?cmp))
   (house-occupied (empty ?e))
   (composed-false (value ?cmp))
    =>
    (?b notify ?d ?a ?c)
)


(defrule rule-stageOfTheDay
   (day-state-change-action (stage ?s) (action ?a) (consequence ?c) (division ?d) (composed ?cmp))/*temperature*/
   (stageOfTheDay (stage ?s))/*temperature*/
    
   (composed-false (value ?cmp)) /*check if it's not a composed rule */
   =>
   (?b notify ?d ?a ?c)
)

(defrule rule-tempDiff
   (temperature-difference-action (value ?v) (action ?a) (consequence ?c) (division ?d) (composed ?cmp) ) /*temp diff*/
   (difference (value ?v))/*temp diff*/
    
   (composed-false (value ?cmp)) /*check if it's not a composed rule */
   =>
    (?b notify ?d ?a ?c)
)

/* --------------- COMPOSED RULES  --------------- */
(defrule rule-time-temperature
   (scheduled-action-with-consequence (hour ?h) (min ?m) (action ?a) (consequence ?c) (division ?d) (composed ?cmp) ) /*time*/
   (current-time (hour ?h) (min ?m) )/*time*/
    
   (action-on-value-temp (value ?v) (action ?a) (consequence ?c) (division ?d) (composed ?cmp))/*temperature*/
   (current-temp (value ?v))/*temperature*/
    
   (composed-true (value ?cmp)) /*check if it's a composed rule */
   =>
    (?b notify ?d ?a ?c)
)

(defrule rule-stageOfTheDay-tempDiff
   (temperature-difference-action (value ?v) (action ?a) (consequence ?c) (division ?d) (composed ?cmp) ) /*temp diff*/
   (difference (value ?v))/*temp diff*/
    
   (day-state-change-action (stage ?s) (action ?a) (consequence ?c) (division ?d) (composed ?cmp))/*stage*/
   (stageOfTheDay (stage ?s))/*stage*/
    
   (composed-true (value ?cmp)) /*check if it's a composed rule */
   =>
    (?b notify ?d ?a ?c)
)