(import logic.houseManager) 
(bind ?b (houseManager.getManager))

(deftemplate current-time (slot hour) (slot min))
(deftemplate house-occupied (slot empty)) /*empty = true || empty = false*/
(deftemplate current-temp (slot value))
(deftemplate current-luminosity (slot value))
(deftemplate current-humidity (slot value))

(deftemplate composed-true (slot value))
(deftemplate composed-false (slot value))

(deftemplate scheduled-action-with-consequence
   (slot hour)
   (slot min)
   (slot action)
   (slot consequence)
   (slot division)
   (slot composed)    
)

(deftemplate action-on-value-temp
   (slot value)
   (slot action)
   (slot consequence)
   (slot division))

(deftemplate action-on-house-empty
   (slot empty)
   (slot action)
   (slot consequence)
   (slot division))

(deftemplate action-on-value-luminosity
   (slot value)
   (slot action)
   (slot consequence)
   (slot division))

(deftemplate action-on-value-humidity
   (slot value)
   (slot action)
   (slot consequence)
   (slot division))
			
/*(bind ?ct (assert (current-time (hour 0) (min 0))))
(bind ?temp (assert (current-temp (value 25))))
(bind ?luminosity (assert (current-luminosity (value 50))))
(bind ?humidity (assert (current-humidity (value 50))))

(assert (scheduled-action-with-consequence (hour 0) (min 5) (action "Wake Up") (consequence "coffe_machine on")  (division "main room")))
(assert (action-on-value-temp (value 15) (action "Temperature reached") (consequence "A/C on") (division "main room")))*/ 
			
(defrule run-scheduled-actions-with-consequence
   (scheduled-action-with-consequence (hour ?h) (min ?m) (action ?a) (consequence ?c) (division ?d) )
   (current-time (hour ?h) (min ?m) )
   =>
   (?b notify ?d ?a ?c ))
   
(defrule run-action-on-value-temp
	(action-on-value-temp (value ?v) (action ?a) (consequence ?c) (division ?d))
	(current-temp (value ?v))
	=>
	(?b notify ?d ?a ?c))

(defrule run-action-on-value-humidity
	(action-on-value-humidity (value ?v) (action ?a) (consequence ?c) (division ?d))
	(current-luminosity (value ?v))
	=>
	(?b notify ?d ?a ?c))

(defrule run-action-on-value-luminosity
	(action-on-value-luminosity (value ?v) (action ?a) (consequence ?c) (division ?d))
	(current-humidity (value ?v))
	=>
	(?b notify ?d ?a ?c))

(defrule run-action-on-house-empty
   (action-on-house-empty (empty ?e) (action ?a) (consequence ?c) (division ?d))
   (house-occupied (empty ?e))
    =>
    (?b notify ?d ?a ?c)
)