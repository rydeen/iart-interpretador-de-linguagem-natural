:-['gramatica'].
:-['analise_semantica'].

%%---------------------------------------------------------------------------------
%%		  _     _            __                    _   _ _ _            _         
%%		 (_)_ _| |_ ___ _ _ / _|__ _ __ ___   _  _| |_(_) (_)_____ _ __| |___ _ _ 
%%		 | | ' \  _/ -_) '_|  _/ _` / _/ -_) | || |  _| | | |_ / _` / _` / _ \ '_|
%%		 |_|_||_\__\___|_| |_| \__,_\__\___|  \_,_|\__|_|_|_/__\__,_\__,_\___/_|  
%%		                                                                          
%%
%%		Leitura do input do utilizador ( a questão colocada à aplicação )
%%
	leituraInputUtilizador( R , Val , Done ):-
	    read_line_to_codes( user_input , Input ),
	    atom_codes(Atom , Input),
	    atomic_list_concat(L, ' ' , Atom),
	    ( inputInvalido(L) -> 	fail ;
	    	  inputStop(L) -> 	( R=stop_program , Done=t ) ;
	    						( frase( R , Val , L , [] ) -> Done=t ; Done=f )
	    ).

	processamentoInputUtilizador( Input , R , Val , Done ):-
		atom_codes( Atom, Input ) ,
		atomic_list_concat( L , ' ', Atom ) ,
		( inputInvalido(L) -> 	fail ;
			  inputStop(L) -> 	R=stop_program ;
	    						( frase( R , Val , L , [] ) -> Done=t ; Done=f )
	    ).

%%
%%		Processamento : análise semântica
%%
	questionaUtilizador(R,Val):-
		nl, write('Em que posso ajuda-lo ?') , nl ,
		leituraInputUtilizador( R , Val , Done) ,
		valido( Done ) , ! .
		


	valido( t ).
	valido( f ):- fail .

	inputInvalido( [] ).
	inputInvalido( [''] ).
	inputInvalido( ['.'] ).

	inputStop([stop]).
	inputStop(['stop.']).
%%
%%---------------------------------------------------------------------------------