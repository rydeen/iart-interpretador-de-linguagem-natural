:-['base_de_conhecimento'].

%%------------------------------------------------------------------------
%%
%%		                   _   /\/|          __     _                   
%%		  __ _ _  _ ___ __| |_|/\/  ___ ___  \_\_  | |__  __ _ ___ ___  
%%		 / _` | || / -_|_-<  _/ _ \/ -_|_-< / _` | | '_ \/ _` (_-</ -_) 
%%		 \__, |\_,_\___/__/\__\___/\___/__/ \__,_| |_.__/\__,_/__/\___| 
%%		    |_|                  _           _               _          
%%		  __| |___   __ ___ _ _ | |_  ___ __(_)_ __  ___ _ _| |_ ___    
%%		 / _` / -_) / _/ _ \ ' \| ' \/ -_) _| | '  \/ -_) ' \  _/ _ \   
%%		 \__,_\___| \__\___/_||_|_||_\___\__|_|_|_|_\___|_||_\__\___/   
%%		                                                                
%%
%%		questões a que a base de conhecimento é capaz de responder
%%	
%%		:------------		 		NOTA 				------------:
%%			A utilização destes predicados e consequentemente dos
%%		predicados da base de conhecimento assumem que esta
%%		se encontra já inicializada !
%%		:------------		 		:--: 				------------:
%%
%%
%%		predicados na base de conhecimento :
%% 			> receita( Prato , Porcao , ModoPreparacao )
%% 			> ingredientes( Prato , ListaIngredientes )
%% 			> prato( Nome , Cidade )
%%
%%
%% 	Devolve os ingredientes para confecionar o prato Prato
	ingredientesPrato( Prato , Resposta ):-
		! , ingredientes( Prato , Resposta ).

%% 	Devolve a receita para confecionar o prato Prato
	receitaPrato( Prato , Resposta ):-
		! , receita( Prato , _ , Resposta ).

%% 	Devolve o numero de porções que a receita do prato Prato serve
	serveReceita( Prato , Resposta ):-
		! , receita( Prato , Resposta , _ ).

%% 	Devolve a regiao do prato Prato
	regiaoPrato( Prato , Resposta ):-
		! , prato( Prato , Resposta ) .

%% 	Devolve o nome dos pratos típicos da regiao Regiao
	pratosRegiao( Regiao , Resposta ):-
		! , findall( Prato , prato(Prato, Regiao) , Resposta ).

%% 	Devolve verdadeiro ou falso. Confere se para a confeção do prato Prato é necessário o ingrediente Ingrediente
	contemIngrediente( Prato , Ingrediente , Resposta ):-
		ingredientes( Prato , ListaIngredientes ),
		( ! , member( Ingrediente-_ , ListaIngredientes ) -> Resposta=[sim] ; Resposta=[nao] ).

%% 	Devolde os pratos que contem o ingrediente Ingrediente
	pratosComIngrediente( Ingrediente , Resposta ):-
		! , findall( Prato , contemIngrediente(Prato , Ingrediente , [sim]) , Resposta1 ) ,
		sort( Resposta1 , Resposta ) .
%%
%%------------------------------------------------------------------------