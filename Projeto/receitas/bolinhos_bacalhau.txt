[1]-[lavar, bem, as, batatas, com, pele, e, levar, a, cozer, ao, vapor, juntamente, com, o, bacalhau, (caso, nao, tenha, panela, de, vapor, pode, cozer, em, agua), '\n'].
[2]-[deixar, arrefecer, tirar, as, peles, e, espinhas, ao, bacalhau, colocar, as, lascas, num, pano, de, cozinha, limpo, fechar, o, pano, e, esfregar, o, bacalhau, ate, que, este, fique, desfeito, em, fios, '\n'].
[]-[reservar, '\n'].
[3]-[pelar, as, batatas, e, esmagar, num, passe, vite, '\n'].
[4]-[deitar, o, pure, da, batata, e, os, fios, de, bacalhau, num, recipiente, fundo, '\n'].
[5]-[juntar, o, ovo, e, as, gemas, a, salsa, picada, o, sal, (se, necessario), e, a, pimenta, '\n'].
[6]-[com, as, maos, amassar, tudo, ate, formar, uma, bola, '\n'].
[7]-[com, as, maos, humidas, formar, bolinhas, ou, se, preferir, pode, usar, duas, colheres, de, sopa, para, obter, o, formato, tradicional, dos, pasteis, de, bacalhau, '\n'].
[8]-[levar, a, fritar, em, oleo, quente, ate, que, fiquem, dourados, '\n'].