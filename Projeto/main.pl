:-['input_utilizador'].

%%==============================================================================================================================%%
%%==============================================================================================================================%%
%%					  _     _                                                _   _       _                   _ 					%%
%%					 | |   (_)_ __   __ _ _   _  __ _  __ _  ___ _ __ ___   | \ | | __ _| |_ _   _ _ __ __ _| |					%%	
%%					 | |   | | '_ \ / _` | | | |/ _` |/ _` |/ _ \ '_ ` _ \  |  \| |/ _` | __| | | | '__/ _` | |					%%
%%					 | |___| | | | | (_| | |_| | (_| | (_| |  __/ | | | | | | |\  | (_| | |_| |_| | | | (_| | |					%%
%%					 |_____|_|_| |_|\__, |\__,_|\__,_|\__, |\___|_| |_| |_| |_| \_|\__,_|\__|\__,_|_|  \__,_|_|					%%
%%					                |___/             |___/                                                    					%%
%%																																%%
%%									  ___    _    ____ _____         ____   ___  _ _  _   										%%
%%									 |_ _|  / \  |  _ \_   _|       |___ \ / _ \/ | || |  										%%
%%									  | |  / _ \ | |_) || |           __) | | | | | || |_ 										%%
%%									  | | / ___ \|  _ < | |          / __/| |_| | |__   _|										%%
%%									 |___/_/   \_\_| \_\|_|         |_____|\___/|_|  |_|  										%%
%%																																%%
%%																																%%
%%																																%%
%%		Projeto:	"Interpretador de linguagem natural, no domínio da gastronomia Portuguesa"									%%
%%		Unidade 	Curricular: Inteligência Artificial (IART)																	%%
%%		Curso: 		Mestrado Integrado em Engenharia Informática e de Computação (MIEIC)										%%
%%		Faculdade: 	Faculdade de Engenharia da Universidade do Porto (FEUP)														%%
%%																																%%
%%		Docente: 			Henrique Lopes Cardoso			hlc@fe.up.pt 														%%
%%		Regente: 			Eugénio da Costa Oliveira		eco@fe.up.pt 														%%
%%																																%%
%%																																%%
%%		Autores: Grupo 42																										%%
%%							Luis Pedro Borges Abreu		ei11146@fe.up.pt 		201106789										%%
%%							Pavel Alexeenko				ei11155@fe.up.pt 		201104331										%%
%%																																%%
%%		Desenvolvimento: Abril e Maio de 2014 	 		Entrega Final: 30 de Maio de 2014										%%
%%																																%%
%%==============================================================================================================================%%
%%==============================================================================================================================%%


%%	Inicializacao da aplicação
%%
%% 	working directory: //Applications/Developer/SourceTree Repositories/iart1314_g42/Projeto
%%
	init:-
		printHeader , nl ,
		changeWorkingDirectory ,
		inicializaBaseDeConhecimento , nl,
		write('O programa pode ser interrompido a qualquer momento utilizando o comando \'stop\'') , nl ,
		write('Para efeitos demonstrativos, o contexto de cada questao sera demonstrado.') , nl , nl ,
		programLoop([],[],[]),
		true .

	programLoop( OldR , OldVal , OldResposta ):-
		printContextoAtual( OldR , OldVal ) ,
	 	! ,
		questionaUtilizador( R , Val ) -> 	
		( 
			R==stop_program -> end_program ;
				( 	! ,
					usarOldR( R , FlagR ) ,
					usarOldVal( Val , FlagVal ) ,

					( 	%% se os dois valores forem novos, significa que a questao é completa e sem contexto anterior
						( flagNew(FlagR) , flagNew(FlagVal) ) -> ( analiseSemantica( R, Val , Resposta ) , nl , programLoop( R , Val , Resposta ) )
						;
						( %% neste caso, é necessário utilizar o contexto anterior
							(
								flagNew(FlagR) -> ( R1=R , calculaNovoVal(R1,OldVal,Val,Val1) ) %% validar contexto de Val
												  ;
											   	  ( (member(OldR,R)->R1=OldR ; R1=[]), calculaNovoVal(R1,OldVal,Val,Val1) ) %% validar contexto de R
							),
							(
								( listavazia(R1) ; listavazia(Val1) ) -> ( analiseSemantica( [], [],    Resposta ) , nl ) ;
																	 	 ( analiseSemantica( R1, Val1 , Resposta ) , nl )
							), 

							( %% se a resposta for invalida, na proxima iteracao sao usados os valores antigos
								semResposta(Resposta) -> programLoop( OldR , OldVal , OldResposta ) ;
			   								   		     programLoop( R1 , Val1 , Resposta ) 
			   				)
						)
					)
			    )
		) ;
		(
			nl, write('Essa questao que me colocou nao e esta correta ou esta fora do contexto do programa.') ,nl ,
			write('Tente novamente ou introduza o comando \'stop\' para terminar o programa.'),nl,nl,
			programLoop( OldR , OldVal , OldResposta )
		).


	end_program:-
		nl,nl,
		write('%%%========================================================================%%%') , nl , 
		write('        Bons cozinhados! Se tiver mais questoes nao hesite em voltar!         ') , nl ,
		write('%%%========================================================================%%%') , nl ,nl , 
		true.

	changeWorkingDirectory:-
		write('Por favor, indique o diretorio da pasta do projeto.') , nl ,
		write('<exemplo>  \'//Applications/Developer/SourceTree Repositories/iart1314_g42/Projeto\' ') , nl ,
		write('<nota> Tome cuidado em colocar o diretorio entre \' \' !') , nl ,
		write('Diretorio = ') ,
		read( Dir ) ,
		( exists_directory( Dir ) -> working_directory( _ , Dir ) ; 
			( nl , write('* Diretorio invalido, tente novamente *') , nl , nl , changeWorkingDirectory ) ) .

	printHeader:-
		nl ,
		write('%%%========================================================================%%%') , nl , 
		write('%%%     Interpretador de Linguagem Natural no dominio da gastronomia PT    %%%') , nl ,
		write('%%%                                                                        %%%') , nl ,
		write('%%%                 MIEIC    FEUP    IART    2013/2014                     %%%') , nl ,
		write('%%%                                                                        %%%') , nl ,
		write('%%%   Grupo 42:   Luis Abreu              ei11146@fe.up.pt                 %%%') , nl ,
		write('%%%               Pavel Alexeenko         ei11155@fe.up.pt                 %%%') , nl ,
		write('%%%========================================================================%%%') , nl .

	printContextoAtual(R,Val):-
		write('------------------------------------------------------------------------------') , nl ,
		write('>>> Contexto atual : ') , nl , 
		write('     1. Objetivo da questao anterior = ') , write(R) , nl ,
		write('     2. Objetos utilizados = ') , write(Val) , nl .
