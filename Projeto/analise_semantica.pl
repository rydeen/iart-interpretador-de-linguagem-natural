:-['questoes'].
:-['formato_resposta'].


continuarExecucao( doit ).


%%------------------------------------------------------------------------------
%%
%%		              __ _ _                          /\      _   _         
%%		  __ _ _ _  _/_/| (_)___ ___   ___ ___ _ __  |/\| _ _| |_(_)__ __ _ 
%%		 / _` | ' \/ _` | | (_-</ -_) (_-</ -_) '  \/ _` | ' \  _| / _/ _` |
%%		 \__,_|_||_\__,_|_|_/__/\___| /__/\___|_|_|_\__,_|_||_\__|_\__\__,_|
%%		                                                                    
%%
%% 		> analise de frase
%%
	analiseSemantica( ingredientes , Prato , Resposta ):-
		( ingredientesPrato( Prato , R ) -> Resposta=R ; Resposta=[] ) ,
		( semResposta(Resposta) -> 	semRespostaMensagem ;
									printIngredientes(Resposta) , nl ) .


	analiseSemantica( pratos , Regiao , Resposta ):-
		bd_regiao( Regiao ) ,
		pratosRegiao( Regiao , Resposta ) ,
		( semResposta(Resposta) -> 	semRespostaMensagem ;
									printReg_Prato(Resposta) , nl ) .

	analiseSemantica( pratos , Ingrediente , Resposta ):-
		bd_ingrediente(Ingrediente) ,
		( pratosComIngrediente( Ingrediente , R )  -> Resposta=R ; Resposta=[] ) , 
		( semResposta(Resposta) -> 	semRespostaMensagem ;
									printReg_Prato(Resposta) , nl ) .

	analiseSemantica( pratos , _ , [] ):- semRespostaMensagem .


	analiseSemantica( receita , Prato , Resposta ):-
		( receitaPrato( Prato , R ) -> Resposta=R ; Resposta=[] ) , 
		( semResposta(Resposta) -> 	semRespostaMensagem ;
									printReceita(Resposta) , nl ) .


	analiseSemantica( regiao , Prato , Resposta ):-
		( regiaoPrato( Prato , R ) -> Resposta=R ; Resposta=[] ) , 
		( semResposta(Resposta) -> 	semRespostaMensagem ;
									printList(Resposta) , nl ) .


	analiseSemantica( servir , Prato , Resposta ):-
		( serveReceita( Prato , R ) -> Resposta=R ; Resposta=[] ) , 
		( semResposta(Resposta) -> 	semRespostaMensagem ;
									printList(Resposta) , nl ) .


	analiseSemantica( conter , [ Prato, Ingrediente ] , Resposta ):-
		( contemIngrediente( Prato , Ingrediente , R ) -> Resposta=R ; Resposta=[] ) , 
		( semResposta(Resposta) -> 	semRespostaMensagem ;
									printList(Resposta) , nl ) .
	

	analiseSemantica( ser , [ Prato , Regiao ] , Resposta ):-
		( regiaoPrato( Prato , Regiao ) -> Resposta=[sim] ; Resposta=[nao] ) , 
		( semResposta(Resposta) -> 	semRespostaMensagem ;
									printList(Resposta) , nl ) .

	analiseSemantica( ser , Prato , Resposta ):-
		analiseSemantica( regiao , Prato , Resposta ).


%%
%% 		> analise de frase_interrogativa_copulativaX
%%
	analiseSemantica( pratosCom , [ Ingrediente1 , Ingrediente2 ] , Resposta ):-
		( 
			Ingrediente1==Ingrediente2 ->
				( 	! , write('Deu-me dois ingredientes iguais. Pretende continuar mesmo assim? [sim-nao]'), nl ,
					read( Input ) ,
					( 
						( 
							Input=nao -> ( write('Reformule a questao novamente.') , nl , nl , Flag=stop ) 
										; ( Input=sim -> (write('Muito bem, vamos continuar.'), nl , nl , Flag=doit )
										  ; ( write('Nao percebi. Vou assumir que foi um nao.') , nl , nl , Flag=stop ))
						)
					)
				) 
				; Flag=doit	
		) ,
		( continuarExecucao( Flag ) ->  (  pratosComIngrediente( Ingrediente1 , L1 ) , 
										   pratosComIngrediente( Ingrediente2 , L2 ) ,
										   inter( L1 , L2 , Resposta ) ,
										   ( semResposta(Resposta) -> 	semRespostaMensagem ;
																		printReg_Prato(Resposta) , nl )
										)
									 ;
									 ( Resposta=[] , nl ) 
		) .


	analiseSemantica( pratosDeCom , [ Regiao , [ Ing ] ] , Resposta ):-
		pratosComIngrediente( Ing , L1 ) ,
		inter(L1,L1, R1 ) ,
		findall( P , regiaoPrato( P , Regiao ) , R2 ) ,
		inter( R1 , R2 , Resposta ) ,
		( semResposta(Resposta) -> 	semRespostaMensagem ;
									printReg_Prato(Resposta) , nl ) .

	analiseSemantica( pratosDeCom , [ Regiao , [ Ing1 , Ing2 ] ] , Resposta ):-
		pratosComIngrediente( Ing1 , L1 ) ,
		pratosComIngrediente( Ing2 , L2 ) , 
		inter(L1,L2, R1 ) ,
		findall( P , regiaoPrato( P , Regiao ) , R2 ) ,
		inter( R1 , R2 , Resposta ) ,
		( semResposta(Resposta) -> 	semRespostaMensagem ;
									printReg_Prato(Resposta) , nl ) .

	analiseSemantica( [] , [] , [] ):- 
		write('Nao me pode colocar questoes sem um contexto. Reformule a questao, por favor.') , nl .
%%
%%------------------------------------------------------------------------------


%%------------------------------------------------------------------------------                
%%		  __ _ _  ___ __
%%		 / _` | || \ \ /
%%		 \__,_|\_,_/_\_\
%%		                
%%		predicados auxiliares
%%
%%	> O predicado 'inter' calcula a intersecção entre as duas listas
%%		as listas são o primeiro e segundo argumentos do predicado,
%%		o terceiro argumento é uma lista com o resultado da intersecção.
%%
	inter([], _, []).

	inter([H1|T1], L2, [H1|Res]) :-
	    member(H1, L2),
	    inter(T1, L2, Res).

	inter([_|T1], L2, Res) :-
	    inter(T1, L2, Res).

%%
%%	> verificar se uma lista (uma resposta) é vazia (sem resposta)
%%
	semResposta( [] ) .
	semResposta( [[]] ) .
	semRespostaMensagem:- write('Nao foi possivel encontrar um resposta para a sua questao. Reformule a questao. ') , write(':(') , nl .

%%
%%	Os seguintes predicados são utilizados para um pré-processamento dos dados antes da análise semântica.
%%	São utilizados no ciclo do programa, presente no ficheiro main.pl
%%	
%%	> processa os valores de R e de Val marcando com uma flag quais os valores a utilizar em cada iteração.
%%		. os predicados FlagOld/1 e FlagNew/1 permitem analisar valor da flag gerada.
%%		. o predicado listavazia/1 valida uma lista vazia
%%
	listavazia( [] ) .

	flagOld( useold ).
	flagNew( usenew ).

	usarOldR( [_|_] , useold ).
	usarOldR( _ , usenew ).
	usarOldVal( [prato] , useold ).
	usarOldVal( [_|_] , usenew ).

%%
%%	> Calcula o valor da variavel Val a utilizar na execucao do programa de acordo com o contexto da questao
%% 		. [H|T] representa o OldVal
%% 		. O Val é o valor atual
%% 		. NewVal será o valor a utilizar na analise semantica
%%
	calculaNovoVal( []  , _ , _ , [] ).
	calculaNovoVal( ser , [_] , Val , Val ).

	calculaNovoVal( conter , [H,T] , Val , NewVal ):-
		bd_ingrediente(Val) -> NewVal=[ H , Val ] ; 
		( prato(Val,_)-> NewVal=[ Val , T ] ; NewVal=[] ) .
		
	calculaNovoVal( ser , [_|T] , Val , NewVal ):-
		prato(Val,_) -> NewVal=[ Val , T ] ; NewVal=[] .

	calculaNovoVal( ingredientes , [H,_] , [prato] , NewVal ):-
		prato(H,_) -> NewVal=H ; NewVal=[] .
	calculaNovoVal( ingredientes , Val , [prato] , NewVal ):-
		prato(Val,_) -> NewVal=Val ; NewVal=[] . 

	calculaNovoVal( receita      , [H,_] , [prato] , NewVal ):-
		prato(H,_) -> NewVal=H ; NewVal=[] .
	calculaNovoVal( receita      , Val , [prato] , NewVal ):-
		prato(Val,_) -> NewVal=Val ; NewVal=[] .


	calculaNovoVal( ingredientes , _ , Val , Val ).
	calculaNovoVal( receita      , _ , Val , Val ).
	calculaNovoVal( pratos       , _ , Val , Val ).
	calculaNovoVal( regiao       , _ , Val , Val ).
	calculaNovoVal( servir       , Val , [prato] , NewVal ):-
		prato(Val,_) -> NewVal = Val ; NewVal=[] .
	calculaNovoVal( servir       , _ , Val , Val ).
	calculaNovoVal( pratosCom    , _ , Val , Val ).
	calculaNovoVal( pratosDeCom  , _ , Val , Val ).
	calculaNovoVal( _  , _ , _ , [] ).
%%
%%------------------------------------------------------------------------------