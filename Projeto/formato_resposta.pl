%%------------------------------------------------------------------
%%
%%		   __                    _                  _     _   
%%		  / _|___ _ _ _ __  __ _| |_ ___   _ __ _ _(_)_ _| |_ 
%%		 |  _/ _ \ '_| '  \/ _` |  _/ _ \ | '_ \ '_| | ' \  _|
%%		 |_| \___/_| |_|_|_\__,_|\__\___/ | .__/_| |_|_||_\__|
%%		                                  |_|                 
%%
%%
%%		 Predicados para a impressão dos resultados na consola
%%

%% imprime os elementos de uma lista
	printList( [] ).
	printList( [H] ):- write(H) .
	printList( [H|T] ):-
		write(H) , write(' ') ,
		printList( T ).


%% imprime um elemento no formato 'ingrediente'
	printIng( Nome-Quantidade ):-
		printList( Quantidade ) ,
		write(' ') ,
		printList( Nome ) , nl .


%% imprime uma lista de elementos do tipo 'ingredientes'
	printIngredientes( [] ):- nl .
	printIngredientes( [H|T] ):-
		printIng( H ) ,
		printIngredientes( T ).


%% imprime numeracao ( dos passos de uma receita )
	printNumero( [0] ).
	printNumero( [N] ):-
		integer(N) ,
		( 
			N<10 -> (write(' ') , write(N)) ; write(N)
		) , 
		write( ' - ' ) .


%% imprime um elemento do tipo 'passo' de uma receita
	printPasso( []-Texto ):-
		write( '     ' ) ,
		printList( Texto ) .
	printPasso( Numero-Texto ):-
		printNumero( Numero ) ,
		printList( Texto ) .


%% imprime uma lista de elementos do tipo 'passo'
	printReceita( [] ):- nl .
	printReceita( [H|T] ):-
		printPasso( H ) ,
		printReceita( T ) .


%% imprime uma lista de elementos do tipo 'regiao' ou do tipo 'prato'
	printReg_Prato( [T] ):- printList(T) , nl .
	printReg_Prato( [H|T] ):-
		printList(H) , nl , printReg_Prato(T).

%%
%%------------------------------------------------------------------