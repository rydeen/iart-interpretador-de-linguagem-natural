%%------------------------------------------------------------------------
%%	                              __  _   _           
%%	   __ _ _ __ __ _ _ __ ___   /_/_| |_(_) ___ __ _ 
%%	  / _` | '__/ _` | '_ ` _ \ / _` | __| |/ __/ _` |
%%	 | (_| | | | (_| | | | | | | (_| | |_| | (_| (_| |
%%	  \__, |_|  \__,_|_| |_| |_|\__,_|\__|_|\___\__,_|
%%	  |___/                                           
%%  
%%------------------------------------------------------------------------
%% As frases serão validadas de acordo com as várias fórmulas admitidas.
%%------------------------------------------------------------------------
%% parâmetros :
%%		V - verbo
%%		T - tipo de nome (sujeito)
%%          tipo de nome aceite pelo verbo (verbo)
%%      X - pessoa-género  ,  ou  P - pessoa e G - género
%%		M - tipo de verbo
%%
%% 			pessoa
%% 				s:singular / p:plural
%%			género
%% 				f:feminino / m:masculíno / u:indefinido
%%			tipo de verbo
%%				i:imperativo / l:lugar / p:pertença / o:outros
%%
%%------------------------------------------------------------------------
%%------------------------------------------------------------------------
%% 						   FRASES INTERROGATIVAS						%%
%%------------------------------------------------------------------------
%% frase_interrogativa1
%%		> quais / qual <verbo> <determinante> <nome> <determinante> <nome> ?
%% 		exemplo : quais sao os ingredientes da francesinha ?
%%
%%------------------------------------------------------------------------
%% frase_interrogativa2
%%		> como é que se / como se <verbo> <determinante> <nome> ?
%%		exemplo : como é que se fazem os bolinhos de bacalhau ?
%%
%%------------------------------------------------------------------------
%% frase_interrogativa3
%%		> de onde <verbo> <determinante> <nome> ?
%%		exemplo : de onde é a francesinha ?
%%
%%------------------------------------------------------------------------
%% frase_interrogativa4
%%		> <sujeito> <verbo> <nome>/<complemento> ?
%%		exemplo : a francesinha leva cebola ?
%% 		exemplo : a francesinha e do porto ?
%%
%%------------------------------------------------------------------------
%% frase_interrogativa5
%% > v1
%%		> que <nome{pratos/ingredinentes}> <verbo> <nome{ingrediente/prato}> ?
%%		exemplo : que ingredientes leva a francesinha ?
%%		exemplo : que pratos levam frango ?
%% > v2 
%%		> que <nome {pratos}> <verbo {sao}> <complemento> ?
%%		exemplo : que pratos sao do porto ?
%%		
%%		> que <nome {pratos}> <verbo {sao}> <ajetivo> <complemento> ?
%%		exemplo : que pratos sao tipicos do porto ?
%%
%%------------------------------------------------------------------------
%% frase_interrogativa6
%%		> o que se pode / o que se <verbo> com <nome{ingrediente}> ?
%%		exemplo : o que se pode cozinhar com frango ?
%%
%%------------------------------------------------------------------------
%% frase_interrogativa7
%% 		> quantos <verbo> <sujeito{receita}> <complemtento> ?
%% 		exemplo : quantos serve a receita da francesinha ?
%%
%%------------------------------------------------------------------------
%%------------------------------------------------------------------------
%% 					FRASES INTERROGATIVAS COPULATIVAS					%%
%%------------------------------------------------------------------------
%% frase_interrogativa_copulativa1	:
%%		> que <nome{pratos}> <verbo> <nome{ingrediente 1}> e <nome{ingrediente 2}> ?
%%		exemplo : que pratos levam bacalhau e cebola ?
%%
%%------------------------------------------------------------------------
%% frase_interrogativa_copulativa2	:	extensão da frase_interrogativa6
%%		> o que se pode / o que se <verbo> com <nome{ingrediente}> e <nome{ingrediente}> ?
%%		exemplo : o que se pode cozinhar com frango e coentros ?
%%
%%------------------------------------------------------------------------
%% frase_interrogativa_copulativa3
%%		> que <nome{pratos}> <verbo> <complemento> e <verbo> <nome{ingrediente}> ?
%%		exemplo : que pratos sao do porto e levam frango ?
%%
%%		> que <nome{pratos}> <verbo> <complemento> e <verbo> <nome{ingrediente}> e <nome{ingrediente}> ?
%%		exemplo : que pratos sao do porto e levam frango e cebola ?
%%
%%		> que <nome{pratos}> <complemento> <verbo> <nome{ingrediente}> ?
%%		exemplo : que pratos do porto levam frango ?
%%
%%		> que <nome{pratos}> <complemento> <verbo> <nome{ingrediente}> e <nome{ingrediente}>?
%%		exemplo : que pratos do porto levam frango e cebola ?
%%
%%------------------------------------------------------------------------
%% frase_interrogativa_copulativa4
%%		> que <nome{pratos}> <verbo> <nome{ingrediente}> e <verbo> <complemento> ?
%%		exemplo : que pratos levam bacalhau e sao do porto ?
%%
%%		> que <nome{pratos}> <verbo> <nome{ingrediente}> e <nome{ingrediente}> e <verbo> <complemento> ?
%%		exemplo : que pratos levam bacalhau e cebola e sao do porto ?
%%
%%------------------------------------------------------------------------
%% frase_interrogativa_copulativa5	:	extensão da frase_interrogativa4
%%		> sujeito{prato} <verbo> <nome{ingrediente 1}> e <nome{ingrediente 2}> ?
%%		exemplo : a francecsinha leva frango e cebola ?
%%
%%------------------------------------------------------------------------
%%------------------------------------------------------------------------
%% 							 FRASES COPULATIVAS							%%
%%------------------------------------------------------------------------
%%	frase_copulativa1
%%		> e <sujeito{prato}> ?
%%		exemplo : <contexto, p.e.> a francesinha e do porto ?
%%					e o bacalhau a bras ?
%%
%%------------------------------------------------------------------------
%%	frase_copulativa2
%%		> e <nome{ingrediente}> ?
%%		exemplo : <contexto, p.e.> a francesinha leva picante ? 
%%					e cebola ?
%%
%%------------------------------------------------------------------------
%%	frase_copulativa3
%%		> e <complemento{regiao}> ?
%%		exemplo : <contexto, p.e.> quais sao os pratos tipicos do porto ? 
%%					e de braga ?
%%
%%------------------------------------------------------------------------
%%	frase_copulativa4
%%		> e quantos <verbo> ?
%%		exemplo : <contexto, p.e.> qual e a receita da francesinha ? 
%%					e quantos serve ?
%%
%%------------------------------------------------------------------------
%%	frase_copulativa5
%%		> e qual / quais <verbo> <sujeito{receita;ingredientes}> ? 
%%		exemplo : <contexto, p.e.> a francesinha e do porto ?
%%					e qual e a receita ? e quais sao os ingredientes ?
%%
%%------------------------------------------------------------------------
%%	frase_copulativa6
%%		> e <complemento{prato}> ? 
%%		exemplo : <contexto, p.e.> qual e a receita do bacalhau a bras ?
%%					e da francesinha ?
%%
%%------------------------------------------------------------------------
%%
%%	  _ _ ___ __ _ _ _ __ _ ___
%%	 | '_/ -_) _` | '_/ _` (_-<
%%	 |_| \___\__, |_| \__,_/__/
%%	         |___/             
%%
%%		R : operação a aplicar
%%		Val : sujeito(s) / objeto(s) 
%%
	frase( R , Val ) --> frase_aux( R1 , Val ) , { ((R1==levar ; R1==ter ; R1==conter) -> R=conter ; R=R1 ) } .

	frase_aux(R,Val) --> frase_interrogativa1(T,_,Val) , { R=T } .
	frase_aux(R,Val) --> frase_interrogativa2(_,Val) , { R=receita } .
	frase_aux(R,Val) --> frase_interrogativa3(T,_,Val) , { T=prato , R=regiao } .
	frase_aux(R,Val) --> frase_interrogativa4(T,V,Val1,Val2) , { T=prato , Val=[Val1,Val2] , R=V } .
	frase_aux(R,Val) --> frase_interrogativa5(_,Val,R) .
	frase_aux(R,Val) --> frase_interrogativa6(_,Val) , { R=pratos }.
	frase_aux(R,Val) --> frase_interrogativa7(R,Val) . 

	frase_aux(R,Val) --> frase_interrogativa_copulativa1(_,_,Val1,Val2) , { R=pratosCom , Val=[Val1,Val2] } .
	frase_aux(R,Val) --> frase_interrogativa_copulativa2(_,_,Val1,Val2) , { R=pratosCom , Val=[Val1,Val2] } .
	frase_aux(R,Val) --> frase_interrogativa_copulativa3(_,_,Val1,Val2) , { R=pratosDeCom , Val=[Val1,Val2] } .
	frase_aux(R,Val) --> frase_interrogativa_copulativa4(_,_,Val1,Val2) , { R=pratosDeCom , Val=[Val1,Val2] } .
	frase_aux(R,Val) --> frase_interrogativa_copulativa5(_,_,Val1,Val2) , { R=pratosDeCom , Val=[Val1,Val2] } .

	%% a parte que nao se sabe vai como lista vazia. Para a analise semantica manda-se o valor da frase anterior.
	frase_aux(R,Val) --> frase_copulativa1(_,_,Val) , { R=[regiao, servir, ser] } .
	frase_aux(R,Val) --> frase_copulativa2(_,_,Val) , { R=[pratos, conter] } .
	frase_aux(R,Val) --> frase_copulativa3(_,_,Val) , { R=[pratos] } .
	frase_aux(R,Val) --> frase_copulativa4(_,V,_)   , { R=V , Val=[prato] } .
	frase_aux(R,Val) --> frase_copulativa5(T,_,_)   , { R=T , Val=[prato] } .
	frase_aux(R,Val) --> frase_copulativa6(_,_,Val) , { R=[receita, ingredientes] } .


%% frase_interrogativaX
	frase_interrogativa1(T,V,R2)    --> pronome_qual(X1), predicado(X1,V-M,T,_), complemento(_,T,R2), pi , {M==o , \+(V==ter) }.
	frase_interrogativa2(V,R)     	--> pronome_como, predicado(_,V-M,_,R), pi , {M==i}.
	frase_interrogativa3(T,V,R)     --> pronome_deOnde, predicado(_,V-M,T,R), pi , {M==l}.
	frase_interrogativa4(T,V,R1,R2) --> sujeito(X,T,R1), verbo(X,V-M,T), nome(_,T2,R2), pi , {T2==ingrediente, M==p , T==prato} .
	frase_interrogativa4(T,V,R1,R2) --> sujeito(X,T,R1), verbo(X,V-M,T), complemento(_,T,R2), pi , {T==prato, M==l} .
	frase_interrogativa5(V,R2,T) 	--> pronome_que, nome(_,T,_), predicado(_,V-M,T2,R2), pi , {T==ingredientes, M==p, T2==prato}.
	frase_interrogativa5(V,R2,T2) 	--> pronome_que, nome(_,T2,_), verbo(P-_,V-M,T), nome(_,T,R2), pi , {\+(P==s) , T==ingrediente, T2==pratos, M==p} .
	frase_interrogativa5(V,R2,T) 	--> pronome_que, nome(_,T,_), verbo(P-_,V-M,_), complemento(_,T,R2), pi , {T==pratos, M==l, P==p}.
	frase_interrogativa5(V,R2,T)	--> pronome_que, nome(_,T,_), verbo(P-_,V-M,_), adjetivo(P-G,T2), complemento(_,T,R2), pi , {T==pratos, T2==tipico, M==l, P==p, G==m}.
	frase_interrogativa6(V,R)   	--> pronome_oQue(_-M) , verbo(P-_,V-M,_) , preposicao_com , nome(_,T,R) , pi , {P==s , T==ingrediente}.
	frase_interrogativa7(V,R2) 		--> pronome_quantos , verbo(_,V-_,T) , sujeito(_,T2,_) , complemento(_,T2,R2) , pi , {T==porcoes , T2==receita} . 

%% frase_interrogativa_copulativaX
	frase_interrogativa_copulativa1(T,V,R2,R3) --> pronome_que, nome(_,pratos,_), verbo(P-_,V-M,T), nome(_,T,R2), particula_copulativa , nome(_,T,R3) , pi ,  {\+(P==s) , T==ingrediente , M==p } .
	frase_interrogativa_copulativa2(T,V,R1,R2) --> pronome_oQue(_-M) , verbo(P-_,V-M,_) , preposicao_com , nome(_,T,R1), particula_copulativa , nome(_,T,R2) , pi , {P==s, T==ingrediente}.
	frase_interrogativa_copulativa3(T1,V,R2,[R3]) --> pronome_que , nome(_,T1,_) , verbo(_,V1-M1,T1) , complemento(_,T1,R2) , particula_copulativa, verbo(P-_,V-M2,T2) , nome(_,T2,R3) , pi , {T1==pratos , T2==ingrediente, \+(P==s), M1==o , M2==p , V1=ser} .
	frase_interrogativa_copulativa3(T1,V,R2,[R3,R4]) --> pronome_que , nome(_,T1,_) , verbo(_,V1-M1,T1) , complemento(_,T1,R2) , particula_copulativa, verbo(P-_,V-M2,T2) , nome(_,T2,R3) , particula_copulativa , nome(_,T2,R4) , pi , {T1==pratos , T2==ingrediente, \+(P==s), M1==o , M2==p , V1=ser } .
	frase_interrogativa_copulativa3(T1,V,R2,[R3]) --> pronome_que , nome(_,T1,_) , complemento(_,T1,R2) , verbo(P-_,V-M,T2) , nome(_,T2,R3) , pi , {T1==pratos , T2==ingrediente, \+(P==s), M==p} .
	frase_interrogativa_copulativa3(T1,V,R2,[R3,R4]) --> pronome_que , nome(_,T1,_) , complemento(_,T1,R2) , verbo(P-_,V-M,T2) , nome(_,T2,R3) , particula_copulativa , nome(_,T2,R4) , pi , {T1==pratos , T2==ingrediente, \+(P==s), M==p , R3\=R4 } .
	frase_interrogativa_copulativa4(T1,V,R3,[R2]) --> pronome_que , nome(_,T1,_) , verbo(P-_,V-M,T2) , nome(_,T2,R2) , particula_copulativa , verbo(_,V1-M1,T1) , complemento(_,T1,R3) , pi , {T1==pratos, T2==ingrediente , \+(P==s) , M==p , M1==o , V1=ser } .
	frase_interrogativa_copulativa4(T1,V,R4,[R2,R3]) --> pronome_que , nome(_,T1,_) , verbo(P-_,V-M,T2) , nome(_,T2,R2) , particula_copulativa , nome(_,T2,R3) , particula_copulativa , verbo(_,V1-M1,T1) , complemento(_,T1,R4) , pi , {T1==pratos, T2==ingrediente , \+(P==s) , M==p , M1==o , V1=ser } .
	frase_interrogativa_copulativa5(T,V,R1,[R2,R3]) --> sujeito(X,T,R1), verbo(X,V-M,T), nome(_,T2,R2), particula_copulativa , nome(_,T2,R3) , pi , {T2==ingrediente, M==p, T==prato } .

%% frase_copulativaX
	frase_copulativa1(T,_,R) --> particula_copulativa , sujeito(_,T,R) , pi , {T==prato}.
	frase_copulativa2(T,_,R) --> particula_copulativa , nome(_,T,R) , pi , {T==ingrediente}.
	frase_copulativa3(T,_,R) --> particula_copulativa , complemento(_,T,R) , pi , {T==pratos}.
	frase_copulativa4(T,V,_) --> particula_copulativa , pronome_quantos , verbo(_,V-_,T) , pi , {T==porcoes}.
	frase_copulativa5(T,V,_) --> particula_copulativa , pronome_qual(X) , predicado(X,V-M,T,_) , pi , {M==o , (T==receita ; T==ingredientes)}.
	frase_copulativa6(T,_,R) --> particula_copulativa , complemento(_,T,R) , pi , {T==receita}.

%% predicado
	predicado(X,V-M,T,R) 	--> verbo(X,V-M,T) , sujeito(X,T,R).

%% sujeito
	sujeito(X,T,R)	--> determinante1(X) , nome(X,T,R) .
	sujeito(X,T,R)  --> determinante1(X) , nome_adjetivo(X,T,R) .

%% nome_adjetivo
	nome_adjetivo(X,pratos,R)  --> nome(X,pratos,R) , adjetivo(X,_) .

%% complemento
	complemento(X,prato,R)        --> determinante2(X) , nome(X,regiao,R) . 
	complemento(X,pratos,R)       --> determinante2(X) , nome(X,regiao,R) . 
	complemento(X,receitas,R)     --> determinante2(X) , nome(X,regiao,R) .
	complemento(X,ingredientes,R) --> determinante2(X) , nome(X,prato,R) . 
	complemento(X,receita,R)      --> determinante2(X) , nome(X,prato,R) . 

%% ponto de interrogação
	pi --> [?].
%%
%%------------------------------------------------------------------------

%%------------------------------------------------------------------------                                  
%%	  _ __   ___  _ __ ___   ___  ___ 
%%	 | '_ \ / _ \| '_ ` _ \ / _ \/ __|
%%	 | | | | (_) | | | | | |  __/\__ \
%%	 |_| |_|\___/|_| |_| |_|\___||___/
%%                                  
	nome(s-f,prato,R) 	   	 --> [francesinha],					  	{ R=[francesinha] } .
	nome(s-m,prato,R)        --> [bacalhau] , [a] , [bras] , 	  	{ R=[bacalhau,a,bras] }.
	nome(p-m,prato,R) 	   	 --> [bolinhos] , [de] , [bacalhau] , 	{ R=[bolinhos,de,bacalhau] } .
	nome(s-m,prato,R) 	   	 --> [bacalhau],[a],[gomes],[de],[sa], 	{ R=[bacalhau,a,gomes,de,sa] } .
	nome(s-m,prato,R) 	   	 --> [caldo] , [verde], 				{ R=[caldo,verde] } .
	nome(p-f,prato,R) 	   	 --> [tripas],[a],[moda],[do],[porto], 	{ R=[tripas, a, moda, do, porto] } .
	nome(p-f,prato,R) 	   	 --> [papas] , [de] , [sarrabulho] , 	{ R=[papas, de, sarrabulho] } .
	nome(p-m,pratos,R) 	   	 --> [pratos] , 						{ R=[pratos] }.
	nome(p-m,ingredientes,R) --> [ingredientes] , 					{ R=[ingredientes] }.
	nome(s-m,ingrediente,R)  --> [bacalhau] , 						{ R=[bacalhau] }.
	nome(s-f,ingrediente,R)  --> [ameijoa] , 						{ R=[ameijoa] }.
	nome(p-m,ingrediente,R)  --> [cominhos] , 						{ R=[cominhos] }.
	nome(s-m,ingrediente,R)  --> [frango] , 						{ R=[frango] }.
	nome(p-m,ingrediente,R)  --> [douradinhos] , 					{ R=[douradinhos] }.
	nome(s-f,ingrediente,R)  --> [cebola] , 						{ R=[cebola] }.
	nome(s-f,receita,R)      --> [receita] , 						{ R=[receita] }.
	nome(m-f,receitas,R)     --> [receitas] , 						{ R=[receitas] }.
	nome(s-m,regiao,R) 	     --> [porto] , 							{ R=[porto] }.
	nome(u-_,regiao,R) 	     --> [braga] , 							{ R=[braga] }.
	nome(u-_,regiao,R) 	     --> [lisboa] , 						{ R=[lisboa] }.
%%
%%------------------------------------------------------------------------


%%------------------------------------------------------------------------
%%	      _      _                      _                   _            
%%	   __| | ___| |_ ___ _ __ _ __ ___ (_)_ __   __ _ _ __ | |_ ___  ___ 
%%	  / _` |/ _ \ __/ _ \ '__| '_ ` _ \| | '_ \ / _` | '_ \| __/ _ \/ __|
%%	 | (_| |  __/ ||  __/ |  | | | | | | | | | | (_| | | | | ||  __/\__ \
%%	  \__,_|\___|\__\___|_|  |_| |_| |_|_|_| |_|\__,_|_| |_|\__\___||___/
%%                                                                  
	determinante1(s-f) --> [a].
	determinante1(p-f) --> [as].
	determinante1(s-m) --> [o].
	determinante1(p-m) --> [os].

	determinante2(s-f) --> [da].
	determinante2(p-f) --> [das].
	determinante2(s-m) --> [do].
	determinante2(p-m) --> [dos].
	determinante2(u-_) --> [de].
%%------------------------------------------------------------------------


%%------------------------------------------------------------------------
%%	            _  _      _   _                
%%	   __ _  __| |(_) ___| |_(_)_   _____  ___ 
%%	  / _` |/ _` || |/ _ \ __| \ \ / / _ \/ __|
%%	 | (_| | (_| || |  __/ |_| |\ V / (_) \__ \
%%	  \__,_|\__,_|/ |\___|\__|_| \_/ \___/|___/
%%	            |__/                           
%%
	adjetivo(p-m,tipico) --> [tipicos].
%%
%%------------------------------------------------------------------------


%%------------------------------------------------------------------------
%%	                 _               
%%	 __   _____ _ __| |__   ___  ___ 
%%	 \ \ / / _ \ '__| '_ \ / _ \/ __|
%%	  \ V /  __/ |  | |_) | (_) \__ \
%%	   \_/ \___|_|  |_.__/ \___/|___/
%%
%% outros verbos
	verbo(s-_,  ser-o,     	    receita)      --> [e].
	verbo(p-_,  ser-o,      	ingredientes) --> [sao].
	verbo(p-_,  ser-o,      	receitas)     --> [sao].
	verbo(p-_,  ser-o,      	pratos)       --> [sao].
	verbo(p-_,  ter-o,      	pratos)       --> [tem]. % têm
	verbo(_-_,  servir-o,       porcoes)      --> [serve].
%% verbos de lugar
	verbo(s-_,  ser-l,          prato)        --> [e].
	verbo(p-_,  ser-l,          prato)        --> [sao].
%% verbos imperativos
	verbo(s-_,  fazer-i,        prato)  	  --> [faz].
	verbo(p-_,  fazer-i,        prato)  	  --> [fazem].
	verbo(s-_,  cozinhar-i,     prato)    	  --> [cozinha].
	verbo(p-_,  cozinhar-i,     prato)  	  --> [cozinham].
	verbo(s-_,  preparar-i,     prato)  	  --> [prepara].
	verbo(p-_,  preparar-i,     prato)  	  --> [preparam].
	verbo(s-_,  confecionar-i,  prato)  	  --> [confeciona].
	verbo(p-_,  confecionar-i,  prato)  	  --> [confecionam].
%% verbos de pertença
	verbo(s-_,  levar-p,        ingrediente)  --> [leva].
	verbo(p-_,  levar-p,        ingrediente)  --> [levam].
	verbo(_-_,  ter-p,          ingrediente)  --> [tem].
	verbo(_-_,  conter-p,       ingrediente)  --> [contem].
	verbo(s-_,  levar-p,        prato)        --> [leva].
	verbo(p-_,  levar-p,        prato)        --> [levam].
	verbo(_-_,  ter-p,          prato)        --> [tem].
	verbo(_-_,  conter-p,       prato)        --> [contem].
%% verbos infinitivo impessoal
	verbo(s-_,  fazer-ii,       prato)  	  --> [fazer].
	verbo(s-_,  cozinhar-ii,    prato)  	  --> [cozinhar].
	verbo(s-_,  preparar-ii,    prato)  	  --> [preparar].
	verbo(s-_,  confecionar-ii, prato)		  --> [confecionar].
%%
%% Notas: ________________________________________________________________
%% uma vez que não valem acentos não distinção entre 'tem' e 'têm'
%% uma vez que não valem acentos não distinção entre 'contém' e 'contêm'
%%------------------------------------------------------------------------


%%------------------------------------------------------------------------
%%	  _ __  _ __ ___  _ __   ___  _ __ ___   ___  ___ 
%%	 | '_ \| '__/ _ \| '_ \ / _ \| '_ ` _ \ / _ \/ __|
%%	 | |_) | | | (_) | | | | (_) | | | | | |  __/\__ \
%%	 | .__/|_|  \___/|_| |_|\___/|_| |_| |_|\___||___/
%%	 |_| 
%%
%% qual / quais : pede pessoa { singular ou plural }
	pronome_qual(p-_) --> [quais] .
	pronome_qual(s-_) --> [qual] .
%% como se / como e que se 
	pronome_como      --> [como] , [se] .
	pronome_como      --> [como] , [e] , [que] , [se] .
%% de onde
	pronome_deOnde    --> [de] , [onde] .
%% que
	pronome_que       --> [que] .
%% o que se pode / o que se : pede tipo de verbo (V-M)
	pronome_oQue(_-ii)	  --> [o] , [que] , [se] , [pode] .
	pronome_oQue(_-i)     --> [o] , [que] , [se] .
%% quantos
	pronome_quantos --> [quantos] .

%%
%% particula copulativa
	particula_copulativa --> [e] .
	preposicao_com --> [com] .
%%
%%------------------------------------------------------------------------