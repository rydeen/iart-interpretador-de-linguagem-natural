:-['leitura_ficheiros'].

:-dynamic( ingredientes/2 ).
:-dynamic( receita/3 ).

%%------------------------------------------------------------------------
%%
%%		  _      _    _      _ _             /\/|    
%%		 (_)_ _ (_)__(_)__ _| (_)_____ _ __ |/\/ ___ 
%%		 | | ' \| / _| / _` | | |_ / _` / _/ _` / _ \
%%		 |_|_||_|_\__|_\__,_|_|_/__\__,_\__\__,_\___/
%%		                                 )_)         
%%
%%		Predicado para inicializar a base de conhecimento
%%
	inicializaBaseDeConhecimento:-
		receitasBaseConhecimento ,
		ingredientesBaseConhecimento ,
		bd_ingredientesBaseConhecimento ,
		bd_regioesBaseConhecimento .
%%
%%------------------------------------------------------------------------


%%------------------------------------------------------------------------
%% 		                 _            
%% 		 _ __  _ __ __ _| |_ ___  ___ 
%% 		| '_ \| '__/ _` | __/ _ \/ __|
%% 		| |_) | | | (_| | || (_) \__ \
%% 		| .__/|_|  \__,_|\__\___/|___/
%% 		|_|                           
%%
%%
%%	> prato( Nome , Cidade ).
%%
	prato( [francesinha] , 					[porto] ).
	prato( [tripas, a, moda, do, porto] , 	[porto] ).
	prato( [bacalhau, a, gomes, de, sa] , 	[porto] ).
	prato( [bacalhau, com, broa] , 			[braga] ).
	prato( [bolinhos, de, bacalhau] , 		[braga] ).
	prato( [caldo, verde] , 				[braga] ).
	prato( [papas, de, sarrabulho] , 		[braga] ).
	prato( [ameijoas, a, bulhao, pato] , 	[lisboa] ).
	prato( [bacalhau, a, bras] , 			[lisboa] ).
	prato( [sardinha, assada] , 			[lisboa] ).
%%
%%------------------------------------------------------------------------


%%------------------------------------------------------------------------
%%		  _                          _ _            _            
%%		 (_)_ __   __ _ _ __ ___  __| (_) ___ _ __ | |_ ___  ___ 
%%		 | | '_ \ / _` | '__/ _ \/ _` | |/ _ \ '_ \| __/ _ \/ __|
%%		 | | | | | (_| | | |  __/ (_| | |  __/ | | | ||  __/\__ \
%%		 |_|_| |_|\__, |_|  \___|\__,_|_|\___|_| |_|\__\___||___/
%%		          |___/                                          
%%
%%
%%	> ingredientes( Prato , ListaIngredientes ).
%% 	
%%		Lista de ingredientes : ingrediente-quantidade
%%
	ingredientesBaseConhecimento:-
		cria_Ingrediente( 'ingredientes/francesinha.txt' , 			[francesinha] ) ,
		cria_Ingrediente( 'ingredientes/tripas_moda_porto.txt' , 	[tripas, a, moda, do, porto] ) ,
		cria_Ingrediente( 'ingredientes/bacalhau_gomes_sa.txt' , 	[bacalhau, a, gomes, de, sa] ) ,
		cria_Ingrediente( 'ingredientes/bacalhau_broa.txt' , 		[bacalhau, com, broa] ) ,
		cria_Ingrediente( 'ingredientes/bolinhos_bacalhau.txt' , 	[bolinhos, de, bacalhau] ) ,
		cria_Ingrediente( 'ingredientes/caldo_verde.txt' , 			[caldo, verde] ) ,
		cria_Ingrediente( 'ingredientes/papas_sarrabulho.txt' , 	[papas, de, sarrabulho] ) ,
		cria_Ingrediente( 'ingredientes/ameijoa_bolhao_pato.txt', 	[ameijoas, a, bulhao, pato] ) ,
		cria_Ingrediente( 'ingredientes/bacalhau_bras.txt' , 		[bacalhau, a, bras] ) ,
		cria_Ingrediente( 'ingredientes/sardinha_assada.txt' , 		[sardinha, assada] ) .

	cria_Ingrediente(Diretorio , Nome):-
		open( Diretorio , read , Str ) ,
		read_file( Str , Ingredientes ) ,
		close(Str) ,
		assert( ingredientes(Nome,Ingredientes) ).	
%%
%%------------------------------------------------------------------------


%%------------------------------------------------------------------------
%%		                    _ _            
%%		  _ __ ___  ___ ___(_) |_ __ _ ___ 
%%		 | '__/ _ \/ __/ _ \ | __/ _` / __|
%%		 | | |  __/ (_|  __/ | || (_| \__ \
%%		 |_|  \___|\___\___|_|\__\__,_|___/
%%
%%
%%	> receita( Prato , Porcao , ModoPreparacao ).
%%		
%%		Lista de passos : numero-intrucao
%%
	receitasBaseConhecimento:-
		cria_Receita( 'receitas/francesinha.txt' , 			['Serve 2'], 	[francesinha] ) ,
		cria_Receita( 'receitas/tripas_moda_porto.txt' , 	['Serve 10'], 	[tripas, a, moda, do, porto] ) ,
		cria_Receita( 'receitas/bacalhau_gomes_sa.txt' , 	['Serve 4'], 	[bacalhau, a, gomes, de, sa] ) ,
		cria_Receita( 'receitas/bacalhau_broa.txt' , 		['Serve 4'], 	[bacalhau, com, broa] ) ,
		cria_Receita( 'receitas/bolinhos_bacalhau.txt' , 	['Serve 5'], 	[bolinhos, de, bacalhau] ) ,
		cria_Receita( 'receitas/caldo_verde.txt' , 			['Serve 6'], 	[caldo, verde] ) ,
		cria_Receita( 'receitas/papas_sarrabulho.txt' , 	['Serve 6'], 	[papas, de, sarrabulho] ) ,
		cria_Receita( 'receitas/ameijoa_bolhao_pato.txt', 	['Serve 4'], 	[ameijoas, a, bulhao, pato] ) ,
		cria_Receita( 'receitas/bacalhau_bras.txt' , 		['Serve 6'], 	[bacalhau, a, bras] ) ,
		cria_Receita( 'receitas/sardinha_assada.txt' , 		['Serve 4'], 	[sardinha, assada] ) .

	cria_Receita(Diretorio , Porcao, Nome ):-
		open( Diretorio , read , Str ) ,
		read_file( Str , Receita ) ,
		close(Str) ,
		assert( receita(Nome,Porcao,Receita) ).	
%%
%%------------------------------------------------------------------------

%%------------------------------------------------------------------------
%%
%%		               _ /\/|        
%%		  _ _ ___ __ _(_)/\/  ___ ___
%%		 | '_/ -_) _` | / _ \/ -_|_-<
%%		 |_| \___\__, |_\___/\___/__/
%%		         |___/               
%%
%%
	:-dynamic( bd_regiao/1 ).
	
	bd_regioesBaseConhecimento:-
		findall( R , prato(_,R) , L ) ,
		sort( L , U ) ,
		bd_instancia_regioes(U) .

	bd_instancia_regioes( [] ).	
	bd_instancia_regioes( [H|T] ):-
		assert( bd_regiao(H) ) ,
		bd_instancia_regioes( T ).
%%
%%------------------------------------------------------------------------

%%------------------------------------------------------------------------
%%
%%		  _                      _ _         _          
%%		 (_)_ _  __ _ _ _ ___ __| (_)___ _ _| |_ ___ ___
%%		 | | ' \/ _` | '_/ -_) _` | / -_) ' \  _/ -_|_-<
%%		 |_|_||_\__, |_| \___\__,_|_\___|_||_\__\___/__/
%%		        |___/                                               
%%
%%
	:-dynamic( bd_ingrediente/1 ).

	bd_ingredientesBaseConhecimento:-
		findall( I , (ingredientes(_,Lista),member(I-_,Lista)) , L ), 
		sort( L , U ) ,
		bd_instancia_ingredientes(U).

	bd_instancia_ingredientes( [] ).
	bd_instancia_ingredientes( [H|T] ):-
		assert( bd_ingrediente(H) ) ,
		bd_instancia_ingredientes(T).
%%
%%------------------------------------------------------------------------